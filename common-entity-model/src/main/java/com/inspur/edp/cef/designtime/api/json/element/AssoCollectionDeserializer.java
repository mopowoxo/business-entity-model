/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.io.IOException;

public class AssoCollectionDeserializer extends JsonDeserializer<GspAssociationCollection> {
    public CefFieldDeserializer getCefFieldDeserializer() {
        return cefFieldDeserializer;
    }
    public void setCefFieldDeserializer(CefFieldDeserializer cefFieldDeserializer) {
        this.cefFieldDeserializer = cefFieldDeserializer;
    }
    private CefFieldDeserializer cefFieldDeserializer;

    @Override public GspAssociationCollection deserialize(JsonParser jsonParser,
        DeserializationContext ctxt) throws IOException, JsonProcessingException {
        GspAssociationCollection collection = new GspAssociationCollection();
        GspAssociationDeserializer deserializer = new GspAssociationDeserializer(this.cefFieldDeserializer);
        SerializerUtils.readArray(jsonParser, deserializer, collection);
        return collection;
    }
}
