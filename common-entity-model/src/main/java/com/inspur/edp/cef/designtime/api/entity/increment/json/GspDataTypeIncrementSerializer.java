/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.entity.increment.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.element.increment.GspCommonFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementSerializer;
import com.inspur.edp.cef.designtime.api.entity.increment.AddedEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.CommonEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.DeletedEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.json.PropertyIncrementSerializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeSerializer;
import java.io.IOException;
import java.util.HashMap;
import lombok.var;

public abstract class GspDataTypeIncrementSerializer extends JsonSerializer<CommonEntityIncrement> {
    @Override
    public void serialize(
        CommonEntityIncrement value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(value, gen);
        writeExtendInfo(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(CommonEntityIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.IncrementType, value.getIncrementType().toString());
        switch (value.getIncrementType()) {
            case Added:
                writeAddedIncrement((AddedEntityIncrement) value, gen);
                break;
            case Modify:
                writeModifyIncrement((ModifyEntityIncrement) value, gen);
                break;
            case Deleted:
                writeDeletedIncrement((DeletedEntityIncrement) value, gen);
                break;
            default:
                throw new RuntimeException("增量序列化失败，不存在增量类型" + value.getIncrementType().toString());
        }
    }

    //region Added
    private void writeAddedIncrement(AddedEntityIncrement value, JsonGenerator gen) {
        writeBaseAddedInfo(value, gen);
        writeExtendAddedInfo(value, gen);
    }

    private void writeBaseAddedInfo(AddedEntityIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.AddedDataType);
        GspCommonDataTypeSerializer cmObjectSerializer = getDataTypeSerializer();
        cmObjectSerializer.serialize(value.getAddedDataType(), gen, null);
    }

    protected void writeExtendAddedInfo(AddedEntityIncrement value, JsonGenerator gen) {

    }

    protected abstract GspCommonDataTypeSerializer getDataTypeSerializer();
    //endregion

    //region Modify
    private void writeModifyIncrement(ModifyEntityIncrement value, JsonGenerator gen) {
        writeBaseModifyInfo(value, gen);
        writeExtendModifyInfo(value, gen);
    }

    private void writeBaseModifyInfo(ModifyEntityIncrement value, JsonGenerator gen) {

        HashMap<String, PropertyIncrement> properties = value.getChangeProperties();
        if (properties != null && properties.size() > 0)
            writeProperties(properties, gen);

        HashMap<String, GspCommonFieldIncrement> fields = value.getFields();
        if (fields != null && fields.size() > 0)
            writeElements(fields, gen);

        HashMap<String, CommonEntityIncrement> childEntites = value.getChildEntitis();
        if (childEntites != null && childEntites.size() > 0)
            writeChildEntities(childEntites, gen);
    }

    private void writeProperties(HashMap<String, PropertyIncrement> properties, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.PropertyIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : properties.entrySet()) {

            SerializerUtils.writeStartObject(gen);
            SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

            SerializerUtils.writePropertyName(gen, CefNames.Value);
            PropertyIncrementSerializer serializer = getPropertyIncrementSerializer();
            serializer.serialize(item.getValue(), gen, null);
            SerializerUtils.writeEndObject(gen);

        }
        SerializerUtils.WriteEndArray(gen);
    }

    private void writeElements(HashMap<String, GspCommonFieldIncrement> fields, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.FieldIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : fields.entrySet()) {
            try {
                SerializerUtils.writeStartObject(gen);
                SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

                SerializerUtils.writePropertyName(gen, CefNames.Element);
                GspFieldIncrementSerializer serializer = getFieldSerializer();
                serializer.serialize(item.getValue(), gen, null);
                SerializerUtils.writeEndObject(gen);
            } catch (IOException e) {
                throw new RuntimeException("字段增量序列化失败", e);
            }
        }
        SerializerUtils.WriteEndArray(gen);
    }

    protected abstract GspFieldIncrementSerializer getFieldSerializer();

    private void writeChildEntities(HashMap<String, CommonEntityIncrement> childEntites, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.ChildIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : childEntites.entrySet()) {
            try {
                SerializerUtils.writeStartObject(gen);
                SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

                SerializerUtils.writePropertyName(gen, CefNames.ChildIncrement);
                GspDataTypeIncrementSerializer serializer = getGspDataTypeIncrementSerializer();
                serializer.serialize(item.getValue(), gen, null);
                SerializerUtils.writeEndObject(gen);
            } catch (IOException e) {
                throw new RuntimeException("子实体增量序列化失败", e);
            }
        }
        SerializerUtils.WriteEndArray(gen);
    }

    protected abstract GspDataTypeIncrementSerializer getGspDataTypeIncrementSerializer();

    protected void writeExtendModifyInfo(ModifyEntityIncrement value, JsonGenerator gen) {

    }

    private PropertyIncrementSerializer getPropertyIncrementSerializer() {
        return new PropertyIncrementSerializer();
    }
    //endregion

    //region Deleted
    private void writeDeletedIncrement(DeletedEntityIncrement value, JsonGenerator gen) {
        writeBaseDeletedInfo(value, gen);
        writeExtendDeletedInfo(value, gen);
    }

    private void writeBaseDeletedInfo(DeletedEntityIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.DeletedId, value.getDeleteId());
    }

    protected void writeExtendDeletedInfo(DeletedEntityIncrement value, JsonGenerator gen) {

    }
    //endregion

    protected void writeExtendInfo(CommonEntityIncrement value, JsonGenerator gen) {

    }
}
