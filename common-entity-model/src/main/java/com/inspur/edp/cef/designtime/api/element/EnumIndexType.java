/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.element;

/**
 * The Definition Of EnumIndexType
 *
 * @ClassName: EnumIndexType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum EnumIndexType {

        /**
         整型
         */
        Integer(0),
        /**
         字符串
         */
        String(1);
        private int intValue;
        private static java.util.HashMap<java.lang.Integer, EnumIndexType> mappings;

        private synchronized static java.util.HashMap<java.lang.Integer, EnumIndexType> getMappings() {
            if (mappings == null) {
                mappings = new java.util.HashMap<java.lang.Integer, EnumIndexType>();
            }
            return mappings;
        }
        private EnumIndexType(int value)
        {
            intValue = value;
            EnumIndexType.getMappings().put(value, this);
        }
        public int getValue()
        {
            return intValue;
        }

        public static EnumIndexType forValue(int value)
        {
            return getMappings().get(value);
        }


}
