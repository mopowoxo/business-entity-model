/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.AbstractControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.RangeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeRuleNames;
import java.util.HashMap;
import java.util.Map;

/**
 * The Definition Of CommonDataTypeControlRule
 *
 * @ClassName: CommonDataTypeControlRule
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDataTypeControlRule extends RangeControlRule {
    public CommonDataTypeControlRule() {
    }

    //region 字段控制规则
    public Map<String, AbstractControlRule> getFieldControlRules() {
        if (getChildRules().containsKey(CommonDataTypeRuleNames.FieldsRules) == false)
            getChildRules().put(CommonDataTypeRuleNames.FieldsRules, new HashMap<>());
        return getChildRules().get(CommonDataTypeRuleNames.FieldsRules);
    }

    public CommonFieldControlRule getFieldControlRule(String fieldId) {
        if (getFieldControlRules().containsKey(fieldId))
            return (CommonFieldControlRule) getFieldControlRules().get(fieldId);
        return null;
    }
    //endregion

    //region 名称规则
    public ControlRuleItem getNameControlRule() {
        return super.getControlRule(CommonDataTypeRuleNames.Name);
    }

    public void setNameControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonDataTypeRuleNames.Name, ruleItem);
    }
    //endregion

    //region 子表新增规则
    public ControlRuleItem getAddChildEntityControlRule() {
        return super.getControlRule(CommonDataTypeRuleNames.AddChildEntity);
    }

    public void setAddChildEntityControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonDataTypeRuleNames.AddChildEntity, ruleItem);
    }
    //endregion

    //region 子表修改规则
    public ControlRuleItem getModifyChildEntitiesControlRule() {
        return super.getControlRule(CommonDataTypeRuleNames.ModifyChildEntities);
    }

    public void setModifyChildEntitiesControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonDataTypeRuleNames.ModifyChildEntities, ruleItem);
    }
    //endregion

    //region 字段新增规则
    public ControlRuleItem getAddFieldControlRule() {
        return super.getControlRule(CommonDataTypeRuleNames.AddField);
    }

    public void setAddFieldControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonDataTypeRuleNames.AddField, ruleItem);
    }
    //endregion

    //region 字段修改规则
    public ControlRuleItem getModifyFieldsControlRule() {
        return super.getControlRule(CommonDataTypeRuleNames.ModifyFields);
    }

    public void setModifyFieldsControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonDataTypeRuleNames.ModifyFields, ruleItem);
    }
    //endregion
}
