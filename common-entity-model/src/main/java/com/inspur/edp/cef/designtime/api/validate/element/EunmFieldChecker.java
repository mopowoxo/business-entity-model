/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.validate.element;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;
import org.springframework.util.StringUtils;

import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.isLegality;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class EunmFieldChecker {
    private static EunmFieldChecker eunmFieldChecker;

    public static EunmFieldChecker getInstance() {
        if (eunmFieldChecker == null) {
            eunmFieldChecker = new EunmFieldChecker();
        }
        return eunmFieldChecker;
    }

    public final void checkEunm(IGspCommonField field, IGspCommonDataType commonDataType) {
        if(field.getContainEnumValues() == null || field.getContainEnumValues().size() ==0){
            CheckUtil.exception("节点["+commonDataType.getCode()+"]对应枚举字段"+field.getName()+"的枚举项不能为空,请添加！");
        }
        for (GspEnumValue enumValue : field.getContainEnumValues()) {
            if(StringUtils.isEmpty(enumValue.getValue())){
                CheckUtil.exception("节点["+commonDataType.getCode()+"]对应枚举字段"+field.getName()+"的枚举项[" + enumValue.getName() + "]的编号不能为空,请修改！");
            }
            if(StringUtils.isEmpty(enumValue.getName())){
                CheckUtil.exception("节点["+commonDataType.getCode()+"]对应枚举字段"+field.getName()+"的枚举项[" + enumValue.getValue() + "]的名称不能为空,请修改！");
            }
            if (!isLegality(enumValue.getValue())) {
                CheckUtil.exception("节点["+commonDataType.getCode()+"]对应枚举字段"+field.getName()+"的枚举项[" + enumValue.getValue() + "]编号是Java关键字,请修改！");
            }
            if(field.getEnumIndexType() == EnumIndexType.String){
                if(field.getMDataType() != GspElementDataType.String){
                    CheckUtil.exception("节点["+commonDataType.getCode()+"]对应枚举字段"+field.getName()+"的索引类型是[字符串索引],数据类型不是文本类型,请修改！");
                }
                if(StringUtils.isEmpty(enumValue.getStringIndex())){
                    CheckUtil.exception("节点["+commonDataType.getCode()+"]对应枚举字段"+field.getName()+"的枚举项[" + enumValue.getValue() + "]的索引不能为空,请修改！");
                }
            }
            else {
                if(StringUtils.isEmpty(enumValue.getIndex())){
                    CheckUtil.exception("节点["+commonDataType.getCode()+"]对应枚举字段"+field.getName()+"的枚举项[" + enumValue.getValue() + "]的索引不能为空,请修改！");
                }
            }
        }
    }
}
