/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.entity.increment.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.element.increment.GspCommonFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.entity.increment.AddedEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.CommonEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.DeletedEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.json.PropertyIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeDeserializer;
import java.io.IOException;

public abstract class GspDataTypeIncrementDeserializer extends JsonDeserializer<CommonEntityIncrement> {
    @Override
    public CommonEntityIncrement deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(jsonParser);
        String incrementTypeStr = node.get(CefNames.IncrementType).textValue();
        if(incrementTypeStr == null || "".equals(incrementTypeStr))
            return null;
        IncrementType incrementType = IncrementType.valueOf(incrementTypeStr);
        switch (incrementType){
            case Added:
                return readAddIncrementInfo(node);
            case Modify:
                return readModifyIncrementInfo(node);
            case Deleted:
                return readDeletedIncrementInfo(node);

        }
        return null;
    }

    //region Add
    private AddedEntityIncrement readAddIncrementInfo(JsonNode node){
        AddedEntityIncrement addIncrement = new AddedEntityIncrement();
        readBaseAddedInfo(addIncrement,node);
        readExtendAddedInfo(addIncrement, node);
        return addIncrement;
    }

    private void readBaseAddedInfo(AddedEntityIncrement addIncrement, JsonNode node){
        JsonNode addVauleNode = node.get(CefNames.AddedDataType);
        if(addVauleNode == null)
            return;

        GspCommonDataTypeDeserializer deserializer = getDataTypeDeserializer();
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(IGspCommonDataType.class, deserializer);
        mapper.registerModule(module);
        try {
            addIncrement.setAddedDataType((GspCommonDataType)mapper.readValue(mapper.writeValueAsString(addVauleNode), IGspCommonDataType.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException("IGspCommonDataType反序列化失败", e);
        } catch (IOException e) {
            throw new RuntimeException("IGspCommonDataType反序列化失败", e);
        }
    }
    protected abstract GspCommonDataTypeDeserializer getDataTypeDeserializer();

    protected void readExtendAddedInfo(AddedEntityIncrement value, JsonNode node) {

    }
    //endregion


    //region Modify
    private ModifyEntityIncrement readModifyIncrementInfo(JsonNode node){
        ModifyEntityIncrement increment = createModifyIncrement();
        readBaseModifyInfo(increment,node);
        readExtendModifyInfo(increment, node);
        return increment;
    }

    protected ModifyEntityIncrement createModifyIncrement(){
        return new ModifyEntityIncrement();
    }

    private void readBaseModifyInfo(ModifyEntityIncrement increment, JsonNode node){
        JsonNode propIncrements = node.get(CefNames.PropertyIncrements);
        if(propIncrements != null)
            readPropertyIncrementsInfo(increment, propIncrements);

        JsonNode eleIncrements = node.get(CefNames.FieldIncrements);
        if(eleIncrements != null)
            readElementIncrementsInfo(increment, eleIncrements);

        JsonNode childIncrements = node.get(CefNames.ChildIncrements);
        if(childIncrements != null)
            readChildIncrementsInfo(increment, childIncrements);
    }

    private void readPropertyIncrementsInfo(ModifyEntityIncrement increment, JsonNode node){
        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array){
            PropertyIncrementDeserializer serializer = getPropertyIncrementSerializer();
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(PropertyIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(CefNames.Value);

                PropertyIncrement prop = mapper.readValue(mapper.writeValueAsString(valueNode), PropertyIncrement.class);
                increment.getChangeProperties().put(key, prop);
            } catch (JsonProcessingException e) {
                throw new RuntimeException("PropertyIncrement反序列化失败", e);
            } catch (IOException e) {
                throw new RuntimeException("PropertyIncrement反序列化失败", e);
            }
        }
    }

    private PropertyIncrementDeserializer getPropertyIncrementSerializer() {
        return new PropertyIncrementDeserializer();
    }
    private void readElementIncrementsInfo(ModifyEntityIncrement increment, JsonNode node){
        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array){
            GspFieldIncrementDeserializer serializer = getFieldSerializer();
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(GspCommonFieldIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(CefNames.Element);

                GspCommonFieldIncrement ele = mapper.readValue(mapper.writeValueAsString(valueNode), GspCommonFieldIncrement.class);
                ele.setId(key);
                increment.getFields().put(key, ele);
            } catch (JsonProcessingException e) {
                throw new RuntimeException("GspCommonFieldIncrement反序列化失败", e);
            } catch (IOException e) {
                throw new RuntimeException("GspCommonFieldIncrement反序列化失败", e);
            }
        }
    }

    protected abstract GspFieldIncrementDeserializer getFieldSerializer();

    private void readChildIncrementsInfo(ModifyEntityIncrement increment, JsonNode node){

        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array){
            GspDataTypeIncrementDeserializer serializer = getChildEntityDeserializer();
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(CommonEntityIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(CefNames.ChildIncrement);

                CommonEntityIncrement childEntity = mapper.readValue(mapper.writeValueAsString(valueNode), CommonEntityIncrement.class);
                childEntity.setId(key);
                increment.getChildEntitis().put(key, childEntity);
            } catch (JsonProcessingException e) {
                throw new RuntimeException("CommonEntityIncrement反序列化失败", e);
            } catch (IOException e) {
                throw new RuntimeException("CommonEntityIncrement反序列化失败", e);
            }
        }

    }


    protected abstract GspDataTypeIncrementDeserializer getChildEntityDeserializer();
    protected void readExtendModifyInfo(ModifyEntityIncrement value, JsonNode node) {

    }
    //endregion

    //region Delete

    private DeletedEntityIncrement readDeletedIncrementInfo(JsonNode node){
        JsonNode deleteIdNode = node.get(CefNames.DeletedId);
        if(deleteIdNode == null)
            return null;
        String deleteId = deleteIdNode.textValue();
        DeletedEntityIncrement increment = new DeletedEntityIncrement(deleteId);
        readExtendDeletedInfo(increment, node);
        return increment;
    }
    protected void readExtendDeletedInfo(DeletedEntityIncrement value, JsonNode node) {

    }

    //endregion


    private ArrayNode trans2Array(JsonNode node){
        try {
            return new ObjectMapper().treeToValue(node, ArrayNode.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("JsonNode转化失败，检查Json结构", e);
        }
    }
}
