/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.collection;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;

/**
 * The Colleciton Of Association
 *
 * @ClassName: GspAssociationCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssociationCollection extends BaseList<GspAssociation> implements Cloneable {

    private static final long serialVersionUID = 1L;

    public final GspAssociation getItem(String id) {
        for (GspAssociation association : this) {
            if (association.getId().equals(id)) {
                return association;
            }
        }
        return null;
    }

    /**
     * 克隆
     *
     * @return
     */
    public final GspAssociationCollection clone() {
        GspAssociationCollection newCollection = new GspAssociationCollection();
        for (GspAssociation item : this) {
            Object tempVar = item.clone();
            newCollection.add((GspAssociation) ((tempVar instanceof GspAssociation) ? tempVar : null));
        }
        return newCollection;
    }

    /**
     * 克隆
     *
     * @param element
     * @return
     */
    public final GspAssociationCollection clone(IGspCommonField element) {
        GspAssociationCollection newCollection = new GspAssociationCollection();
        for (GspAssociation item : this) {
            newCollection.add(item.clone(element));
        }
        return newCollection;
    }

}
