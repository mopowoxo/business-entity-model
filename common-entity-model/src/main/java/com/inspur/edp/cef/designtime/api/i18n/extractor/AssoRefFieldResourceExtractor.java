/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.i18n.extractor;


import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;

public abstract class AssoRefFieldResourceExtractor extends CefFieldResourceExtractor {
    private IGspCommonField commonField;

    public AssoRefFieldResourceExtractor(IGspCommonField field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(field, context, parentResourceInfo);
        commonField = field;
    }

    protected CefResourcePrefixInfo buildCurrentPrefix() {
        if (!commonField.getIsRefElement()) {
//			throw new CefI18nExtractException("[多语项抽取]非关联带出字段，不应使用AssoRefFieldResourceExtractor进行抽取。");
            throw new RuntimeException("[多语项抽取]非关联带出字段，不应使用AssoRefFieldResourceExtractor进行抽取。");

        }
        if (getParentResourcePrefixInfo() == null) {
//			throw new CefI18nExtractException("多语项抽取_字段关联信息"+commonField.getParentAssociation()+"无对应的多语前缀信息。");
            throw new RuntimeException("多语项抽取_字段关联信息" + commonField.getParentAssociation() + "无对应的多语前缀信息。");
        }
        CefResourcePrefixInfo fieldResourceInfo = new CefResourcePrefixInfo();
        fieldResourceInfo.setResourceKeyPrefix(getParentResourcePrefixInfo().getResourceKeyPrefix() + "." + commonField.getLabelID());
        fieldResourceInfo.setDescriptionPrefix(getParentResourcePrefixInfo().getDescriptionPrefix() + "的关联带出属性'" + commonField.getName() + "'");
        return fieldResourceInfo;
    }
}
