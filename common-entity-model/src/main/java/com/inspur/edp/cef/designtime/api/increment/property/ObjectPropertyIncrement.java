/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.increment.property;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;

public class ObjectPropertyIncrement extends NativePropertyIncrement{

    public ObjectPropertyIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }
    public Class getPropertyIncrementClass(){
        return Object.class;
    }
    @Override
    public IncrementPropType getPropertyType() {
        return IncrementPropType.Object;
    }

    public JsonSerializer getJsonSerializer(){
        return null;
    }

    public JsonDeserializer getJsonDeserializer(){
        return null;
    }

    public CefFieldDeserializer getCefFieldDeserializer() {
        return cefFieldDeserializer;
    }

    public void setCefFieldDeserializer(CefFieldDeserializer cefFieldDeserializer) {
        this.cefFieldDeserializer = cefFieldDeserializer;
    }

    private CefFieldDeserializer cefFieldDeserializer;

}
