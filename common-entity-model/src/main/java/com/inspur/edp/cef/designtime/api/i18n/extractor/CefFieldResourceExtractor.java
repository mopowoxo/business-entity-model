/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.i18n.extractor;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceDescriptionNames;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceKeyNames;

public abstract class CefFieldResourceExtractor extends AbstractResourceExtractor {
    private IGspCommonField commonField;

    protected CefFieldResourceExtractor(
        IGspCommonField field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(context, parentResourceInfo);
        commonField = field;
    }

    protected final void extractItems() {
        // Name
        addResourceInfo(CefResourceKeyNames.Name, commonField.getName(), CefResourceDescriptionNames.Name);

        //关联枚举
        switch (commonField.getObjectType()) {
            case Association:
                extractAssoInfo(commonField.getChildAssociations());
                break;
            case Enum:
                extractEnumValue(commonField.getContainEnumValues());
                break;
            case None:
                break;
        }

        //扩展
        extractExtendProperties(commonField);
    }

    /**
     * 赋值字段的国际化项前缀
     */
    protected final void setPrefixInfo() {
        commonField.setI18nResourceInfoPrefix(getCurrentResourcePrefixInfo().getResourceKeyPrefix());
    }

    protected CefResourcePrefixInfo buildCurrentPrefix() {
        if (getParentResourcePrefixInfo() == null) {
//            throw new CefI18nExtractException("多语项抽取_节点" + commonField.getBelongObject().getName() + "中字段" + commonField.getName() + "，无节点对应的多语前缀信息。");
            throw new RuntimeException("多语项抽取_节点" + commonField.getBelongObject().getName() + "中字段" + commonField.getName() + "，无节点对应的多语前缀信息。");
        }
        CefResourcePrefixInfo fieldResourceInfo = new CefResourcePrefixInfo();
        fieldResourceInfo.setResourceKeyPrefix(getParentResourcePrefixInfo().getResourceKeyPrefix() + "." + commonField.getLabelID());
        fieldResourceInfo.setDescriptionPrefix(getParentResourcePrefixInfo().getDescriptionPrefix() + "中'" + commonField.getName() + "'属性");
        return fieldResourceInfo;
    }


    //#region 私有方法
    private void extractAssoInfo(GspAssociationCollection assos) {
        if (assos != null && assos.size() > 0) {
            for (GspAssociation item : assos) {
                getAssoResourceExtractor(getContext(), getCurrentResourcePrefixInfo(), item).extract();
            }
        }
    }

    private void extractEnumValue(GspEnumValueCollection enumValues) {
        if (enumValues != null && enumValues.size() > 0) {
            for (GspEnumValue item : enumValues) {
                String keyPropName = item.getValue() + "." + CefResourceKeyNames.DisplayValue;
                String value = item.getName();
                String descriptionPropName = "枚举信息'" + item.getName() + "'的" + CefResourceDescriptionNames.DisplayValue;
                addResourceInfo(keyPropName, value, descriptionPropName);

                String enumKey = getCurrentResourcePrefixInfo().getResourceKeyPrefix() + "." + item.getValue();
                item.setI18nResourceInfoPrefix(enumKey);
            }
        }
    }

    //#endregion

    protected void extractExtendProperties(IGspCommonField commonField) {
    }

    protected abstract AssoResourceExtractor getAssoResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo fieldPrefixInfo, GspAssociation asso);

}
