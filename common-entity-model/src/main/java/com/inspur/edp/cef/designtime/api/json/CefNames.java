/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json;

/**
 * The Serializer Names Of Cef Model
 *
 * @ClassName: CefNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CefNames
{
	public static final String ID = "ID";
	public static final String Code = "Code";
	public static final String Name = "Name";
	public static final String PkgName = "PkgName";
	public static final String LabelID = "LabelID";
	public static final String DataType = "DataType";
	public static final String Length = "Length";
	public static final String Precision = "Precision";
	public static final String DefaultValue = "DefaultValue";
	public static final String DefaultVauleType = "DefaultVauleType";
	public static final String DefaultValueType = "DefaultValueType";
	public static final String IsVirtual = "IsVirtual";
	public static final String IsRequire = "IsRequire";
	public static final String ObjectType = "ObjectType";
	public static final String IsRef = "IsRef";
	public static final String BelongObject = "BelongObject";
	public static final String ChildAssociations = "ChildAssociations";
	public static final String ContainEnumValues = "ContainEnumValues";
	public static final String ContainElements = "ContainElements";
	public static final String DtmAfterCreate = "DtmAfterCreate";
	public static final String DtmAfterModify = "DtmAfterModify";
	public static final String DtmBeforeSave = "DtmBeforeSave";
	public static final String ValAfterModify = "ValAfterModify";
	public static final String ValBeforeSave = "ValBeforeSave";
	public static final String IsMultiLanguage = "IsMultiLanguage";
	public static final String ComponentId = "ComponentId";
	public static final String Description = "Description";
	public static final String ComponentPkgName = "ComponentPkgName";
	public static final String ComponentName = "ComponentName";
	public static final String ChildElements = "ChildElements";
	public static final String MappingRelation = "MappingRelation";
	public static final String HasAssociation = "HasAssociation";
	public static final String IsRefElement = "IsRefElement";
	public static final String RefElementID = "RefElementID";
	public static final String ParentAssociation = "ParentAssociation";
	public static final String RefModelId = "RefModelId";
	public static final String Id = "Id";
	public static final String SourceElement = "SourceElement";
	public static final String SourceElementDisplay = "SourceElementDisplay";
	public static final String TargetElementDisplay = "TargetElementDisplay";
	public static final String TargetElement = "TargetElement";
	public static final String RefDataModelName = "RefDataModelName";
	public static final String RefdataModelName="RefdataModelName";

	public static final String RunOnce = "RunOnce";
	public static final String TriggerPointType = "TriggerPointType";

	public static final String RequestElements = "RequestElements";
	public static final String GetExecutingDataStatus = "GetExecutingDataStatus";
	public static final String ChildTriggerInfo = "ChildTriggerInfo";

	public static final String RequestChildElements = "RequestChildElements";
	public static final String RequestChildElementKey = "RequestChildElementKey";
	public static final String RequestChildCode = "Code";
	public static final String RequestDtmChildElementValue = "Value";
	public static final String RequestChildElementValue = "RequestChildElementValue";

	public static final String CollectionType = "CollectionType";

	public static final String BeLabel="BeLabel";
	//Element
	public static final String IsFromAssoUdt = "IsFromAssoUdt";
	public static final String BillCodeConfig = "BillCodeConfig";
	public static final String ContainChildObjects = "ContainChildObjects";
	public static final String PrimayKeyID = "PrimayKeyID";
	public static final String ExtendNodeList = "ExtendNodeList";
	public static final String ExtProperties = "ExtProperties";
	public static final String Resource = "Resource";
	public static final String ShowMessage = "ShowMessage";
	public static final String Type = "Type";
	public static final String TableName = "TableName";
	public static final String ColumnName = "ColumnName";
	public static final String ContainConstraints = "ContainConstraints";
	public static final String Keys = "Keys";
	public static final String GeneratingAssembly = "GeneratingAssembly";
	public static final String MDataType = "MDataType";
	public static final String DynamicPropSetInfo = "DynamicPropSetInfo";
	public static final String I18nResourceInfoPrefix = "I18nResourceInfoPrefix";
	public static final String CustomizationInfo = "CustomizationInfo";
	// Bug 353424: 需兼容已有元数据，此处不打标签
	public static final String IsCustomized = "Customized";
	public static final String DimensionInfo = "DimensionInfo";
	public static final String FirstDimension = "FirstDimension";
	public static final String FirstDimensionCode = "FirstDimensionCode";
	public static final String FirstDimensionName = "FirstDimensionName";
	public static final String SecondDimension = "SecondDimension";
	public static final String SecondDimensionCode = "SecondDimensionCode";
	public static final String SecondDimensionName = "SecondDimensionName";
	public static final String AssoModelInfo = "AssoModelInfo";
	public static final String Value = "Value";
	public static final String IsDefaultEnum = "IsDefaultEnum";
	public static final String Index = "Index";
	public static final String StringIndex = "StringIndex";
	public static final String EnumIndexType = "EnumIndexType";
	public static final String EnableRtrim = "EnableRtrim";
	public static final String IsBigNumber = "IsBigNumber";
	public static final String BizTagIds = "BizTagIds";
	public static final String DefaultValueInfo = "DefaultValueInfo";
	public static final String DisplayDefaultValue = "DisplayDefaultValue";
	public static final String ChangedProperties="ChangedProperties";


	// Udt相关
	public static final String IsUdt = "IsUdt";
	public static final String UdtPkgName = "UdtPkgName";
	public static final String UdtID = "UdtID";
	public static final String UdtName = "UdtName";
	public static final String UnifiedDataType = "UnifiedDataType";

	//业务字段相关
	public static final String RefBusinessFieldId = "RefBusinessFieldId";
	public static final String RefBusinessFieldName = "RefBusinessFieldName";

	public static final String MappingInfo = "MappingInfo";
	public static final String KeyInfo = "KeyInfo";
	public static final String ValueInfo = "ValueInfo";
	// 关联
	public static final String RefModelID = "RefModelID";
	public static final String RefModelCode = "RefModelCode";
	public static final String RefModelName = "RefModelName";
	public static final String RefModelPkgName = "RefModelPkgName";
	public static final String Asso_RefObjectID = "RefObjectID";
	public static final String Asso_RefObjectCode = "RefObjectCode";
	public static final String Asso_RefObjectName = "RefObjectName";
	public static final String KeyCollection = "KeyCollection";
	public static final String AssociationKey = "AssociationKey";
	public static final String Where = "Where";
	public static final String AssoConditions = "AssoConditions";
	public static final String AssSendMessage = "AssSendMessage";
	public static final String ForeignKeyConstraintType = "ForeignKeyConstraintType";
	public static final String DeleteRuleType = "DeleteRuleType";
	public static final String RefElementCollection = "RefElementCollection";
	public static final String ModelConfigId = "ModelConfigId";
	public static final String MainObjCode = "MainObjCode";
	public static final String keepAssoPropertyForExpression = "keepAssoPropertyForExpression";

	// 变量
	public static final String VariableSourceType = "VariableSourceType";
	// 操作
	public static final String IsGenerateComponent = "IsGenerateComponent";

	//动态属性集合
	public static final String EnableDynamicProp = "EnableDynamicProp";
	public static final String DynamicPropRepositoryComp = "DynamicPropRepositoryComp";
	public static final String DynamicPropSerializerComp = "DynamicPropSerializerComp";
	//原节点上的动态属性-已废弃
	public static final String DynamicPropSerializerComps = "DynamicPropSerializerComps";

	//region Increment
	public static final String IncrementType = "IncrementType";
	public static final String AddedDataType = "AddedDataType";
	public static final String DeletedId = "DeletedId";
	public static final String HasIncrement  = "HasIncrement";
	public static final String PropertyType = "PropertyType";
	public static final String PropertyValue = "PropertyValue";
	public static final String ObjectDeserializer = "ObjectDeserializer";
	public static final String ObjectSerializer = "ObjectSerializer";
	public static final String ObjectIncrementType = "ObjectIncrementType";
	public static final String PropertyIncrements = "PropertyIncrements";
	public static final String FieldIncrements ="FieldIncrements";
	public static final String ChildIncrements = "ChildIncrements";
	public static final String Element = "Element";
	public static final String ChildIncrement = "ChildIncrement";
	//endregion


}
