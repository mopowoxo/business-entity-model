/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.cef.designtime.api.json.CefNames;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DimensionInfo {

    private String firstDimension;

    @JsonProperty(CefNames.FirstDimension)
    public String getFirstDimension() {
        return firstDimension;
    }
    public void setFirstDimension(String firstDimension) {
        this.firstDimension = firstDimension;
    }

    private String secondDimension;

    @JsonProperty(CefNames.SecondDimension)
    public String getSecondDimension() {
        return secondDimension;
    }
    public void setSecondDimension(String secondDimension) {
        this.secondDimension = secondDimension;
    }


    private String firstDimensionCode;

    @JsonProperty(CefNames.FirstDimensionCode)
    public String getFirstDimensionCode() {
        return firstDimensionCode;
    }
    public void setFirstDimensionCode(String firstDimensionCode) {
        this.firstDimensionCode = firstDimensionCode;
    }

    private String secondDimensionCode;

    @JsonProperty(CefNames.SecondDimensionCode)
    public String getSecondDimensionCode() {
        return secondDimensionCode;
    }
    public void setSecondDimensionCode(String secondDimensionCode) {
        this.secondDimensionCode = secondDimensionCode;
    }


    private String firstDimensionName;

    @JsonProperty(CefNames.FirstDimensionName)
    public String getFirstDimensionName() {
        return firstDimensionName;
    }
    public void setFirstDimensionName(String firstDimensionName) {
        this.firstDimensionName = firstDimensionName;
    }


    private String secondDimensionName;

    @JsonProperty(CefNames.SecondDimensionName)
    public String getSecondDimensionName() {
        return secondDimensionName;
    }
    public void setSecondDimensionName(String secondDimensionName) {
        this.secondDimensionName = secondDimensionName;
    }


    public boolean equals(DimensionInfo info){
        if(this.firstDimension == null
        && info.firstDimension == null
        && this.secondDimension == null
        && info.secondDimension == null)
            return true;

        if(this.firstDimension != null
        && this.firstDimension.equals(info.firstDimension)
        && this.secondDimension != null
        && this.secondDimension.equals(info.secondDimension))
            return true;
        return false;
    }
}
