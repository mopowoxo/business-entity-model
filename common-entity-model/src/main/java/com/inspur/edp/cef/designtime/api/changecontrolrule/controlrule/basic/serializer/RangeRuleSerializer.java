/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleNames;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.RangeControlRule;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.io.IOException;
import java.util.Map;

/**
 * The Json Serializer Of RangeRule
 *
 * @ClassName: RangeRuleSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RangeRuleSerializer<T extends RangeControlRule> extends AbstractControlRuleSerializer<T> {
    @Override
    protected final void writeExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeExtendInfos(controlRule, jsonGenerator, serializerProvider);
        writeRangeRules(controlRule, jsonGenerator, serializerProvider);
        writeRangeExtendInfos(controlRule, jsonGenerator, serializerProvider);
    }

    protected void writeRangeExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
//里面不填内容。
    }

    private void writeRangeRules(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writePropertyName(jsonGenerator, ControlRuleNames.RangeControlRules);
        SerializerUtils.writeStartObject(jsonGenerator);
        innerWriteRangeRules(controlRule, jsonGenerator, serializerProvider);
        SerializerUtils.writeEndObject(jsonGenerator);
    }

    private void innerWriteRangeRules(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        if (controlRule.getRangeControlRules() == null || controlRule.getRangeControlRules().size() == 0)
            return;
        for (Map.Entry<String, Map<String, ControlRuleItem>> entry : controlRule.getRangeControlRules().entrySet()) {
            SerializerUtils.writePropertyName(jsonGenerator, entry.getKey());
            SerializerUtils.writeStartObject(jsonGenerator);
            SerializerUtils.WriteStartArray(jsonGenerator);
            writeRangeRuleItems(entry.getValue(), jsonGenerator, serializerProvider);
            SerializerUtils.WriteEndArray(jsonGenerator);
            SerializerUtils.writeEndObject(jsonGenerator);
        }
    }

    private void writeRangeRuleItems(Map<String, ControlRuleItem> value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        if (value == null || value.size() == 0)
            return;
        for (Map.Entry<String, ControlRuleItem> item : value.entrySet()) {
            try {
                jsonGenerator.writeObject(item.getValue());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
