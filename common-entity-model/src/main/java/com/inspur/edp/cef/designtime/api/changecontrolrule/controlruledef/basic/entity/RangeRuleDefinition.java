/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.parser.ControlRuleDefParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer.ControlRuleDefSerializer;
import java.util.HashMap;
import java.util.Map;

/**
 * The Definition Of RangeRuleDefinition
 *
 * @ClassName: RangeRuleDefinition
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = ControlRuleDefSerializer.class)
@JsonDeserialize(using = ControlRuleDefParser.class)
public class RangeRuleDefinition extends ControlRuleDefinition {
    private Map<String, Map<String, ControlRuleDefItem>> rangeRules = new HashMap<>();

    public RangeRuleDefinition(ControlRuleDefinition parentRuleDefinition, String ruleObjectType) {
        super(parentRuleDefinition, ruleObjectType);
    }

    public RangeRuleDefinition() {
    }

    public Map<String, Map<String, ControlRuleDefItem>> getRangeRules() {
        return rangeRules;
    }
}
