/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.parser.ControlRuleDefParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer.ControlRuleDefSerializer;
import com.inspur.edp.cef.designtime.api.util.DataValidator;
import java.util.HashMap;
import java.util.Map;

/**
 * The Definition Of ControlRuleDefinition
 *
 * @ClassName: ControlRuleDefinition
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = ControlRuleDefSerializer.class)
@JsonDeserialize(using = ControlRuleDefParser.class)
public class ControlRuleDefinition {
    private Map<String, ControlRuleDefinition> childControlRules = new HashMap<>();
    private ControlRuleDefinition parentRuleDefinition;
    private String ruleObjectType;
    private Map<String, ControlRuleDefItem> selfControlRules = new HashMap<>();

    public ControlRuleDefinition(ControlRuleDefinition parentRuleDefinition, String ruleObjectType) {
        this.parentRuleDefinition = parentRuleDefinition;
        this.ruleObjectType = ruleObjectType;
    }

    public ControlRuleDefinition() {
    }

    //region ChildRule
    public Map<String, ControlRuleDefinition> getChildControlRules() {
        return childControlRules;
    }
    //endregion

    //region ParentRule
    public boolean hasParentContrulRuleDef() {
        return parentRuleDefinition != null;
    }

    public ControlRuleDefinition getParentRuleDefinition() {
        return parentRuleDefinition;
    }

    public void setParentRuleDefinition(ControlRuleDefinition parentRuleDefinition) {
        this.parentRuleDefinition = parentRuleDefinition;
    }
    //endregion

    //region ObjectType
    public String getRuleObjectType() {
        return ruleObjectType;
    }

    public void setRuleObjectType(String ruleObjectType) {
        this.ruleObjectType = ruleObjectType;
    }
    //endregion

    //region ControlRuleDefItem
    public Map<String, ControlRuleDefItem> getSelfControlRules() {
        return selfControlRules;
    }

    public ControlRuleDefItem getControlRuleItem(String ruleName) {
        if (!getSelfControlRules().containsKey(ruleName)) {
            throw new RuntimeException(String.format("未能找到规则名称为%1$s的规则。", ruleName));
        }
        ControlRuleDefItem ruleDefItem = getSelfControlRules().get(ruleName);
        if (ruleDefItem == null)
            throw new RuntimeException(String.format("根据名称获取到的规则名称为%1$s的规则为空，请了解。", ruleName));
        return ruleDefItem;
    }

    public void setControlRuleItem(String ruleName, ControlRuleDefItem ruleItem) {
        DataValidator.checkForNullReference(ruleItem, "规则的值");
        DataValidator.checkForEmptyString(ruleName, "规则名称");
        getSelfControlRules().put(ruleName, ruleItem);
    }

    //endregion
}
