/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer.ControlRuleDefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.io.IOException;

/**
 * The Json Parser Of ControlDefItem
 *
 * @ClassName: ControlDefItemParser
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlDefItemParser  extends JsonDeserializer<ControlRuleDefItem> {
    @Override
    public ControlRuleDefItem deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        SerializerUtils.readStartObject(jsonParser);
        ControlRuleDefItem item =new ControlRuleDefItem();
        while (jsonParser.getCurrentToken()== JsonToken.FIELD_NAME)
        {
            String propertyName=  SerializerUtils.readPropertyName(jsonParser);
            switch (propertyName)
            {
                case ControlRuleDefNames.Description:
                    item.setDescription(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case ControlRuleDefNames.DefaultRuleValue:
                    item.setDefaultRuleValue(SerializerUtils.readPropertyValue_Enum(jsonParser, ControlRuleValue.class,ControlRuleValue.values()));
                    break;
                case ControlRuleDefNames.RuleDisplayName:
                    item.setRuleDisplayName(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case ControlRuleDefNames.RuleName:
                    item.setRuleName(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                    default:
                        throw new RuntimeException(String.format("找不到属性%1$s",propertyName));
            }
        }
        SerializerUtils.readEndObject(jsonParser);
        return item;
    }
}
