/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.AbstractControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.io.IOException;
import java.util.Map;

/**
 * The Abstract Json Serializer Of ControlRule
 *
 * @ClassName: AbstractControlRuleSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class AbstractControlRuleSerializer<T extends AbstractControlRule> extends JsonSerializer<T> {
    @Override
    public void serialize(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        SerializerUtils.writeStartObject(jsonGenerator);
        writeSelfInfos(controlRule, jsonGenerator, serializerProvider);
        writeExtendInfos(controlRule, jsonGenerator, serializerProvider);
        SerializerUtils.writeEndObject(jsonGenerator);
    }

    protected void writeExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writePropertyValue(jsonGenerator, ControlRuleNames.RuleId, controlRule.getRuleId());
        SerializerUtils.writePropertyValue(jsonGenerator, ControlRuleNames.RuleDefId, controlRule.getRuleDefId());
        writeSelfControlRules(controlRule, jsonGenerator, serializerProvider);
        writeChildControlRules(controlRule, jsonGenerator, serializerProvider);
    }


    private void writeSelfControlRules(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writePropertyName(jsonGenerator, ControlRuleNames.SelfControlRules);
        SerializerUtils.WriteStartArray(jsonGenerator);
        innerWriteSelfControlRules(controlRule, jsonGenerator, serializerProvider);
        SerializerUtils.WriteEndArray(jsonGenerator);
    }

    private void innerWriteSelfControlRules(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        if (controlRule.getSelfControlRules() == null || controlRule.getSelfControlRules().size() == 0)
            return;
        for (Map.Entry<String, ControlRuleItem> item : controlRule.getSelfControlRules().entrySet()) {
            SerializerUtils.writeStartObject(jsonGenerator);
            SerializerUtils.writePropertyValue(jsonGenerator, "key", item.getKey());
            SerializerUtils.writePropertyName(jsonGenerator, "value");
            SerializerUtils.writePropertyValue_Object(jsonGenerator, item.getValue());
            SerializerUtils.writeEndObject(jsonGenerator);
        }
    }


    private void writeChildControlRules(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writePropertyName(jsonGenerator, ControlRuleNames.ChildControlRules);
        SerializerUtils.WriteStartArray(jsonGenerator);
        innerWriteChildControlRules(controlRule, jsonGenerator, serializerProvider);
        SerializerUtils.WriteEndArray(jsonGenerator);
    }

    private void innerWriteChildControlRules(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        if (controlRule.getChildRules() == null || controlRule.getChildRules().size() == 0)
            return;
        for (Map.Entry<String, Map<String, AbstractControlRule>> item : controlRule.getChildRules().entrySet()) {
            SerializerUtils.writeStartObject(jsonGenerator);
            SerializerUtils.writePropertyValue(jsonGenerator, ControlRuleNames.RoleType, item.getKey());
            SerializerUtils.writePropertyName(jsonGenerator, ControlRuleNames.RoleValues);
            SerializerUtils.WriteStartArray(jsonGenerator);
            innerWriteChildControlRules(item, jsonGenerator, serializerProvider);
            SerializerUtils.WriteEndArray(jsonGenerator);
            SerializerUtils.writeEndObject(jsonGenerator);
        }
    }

    private void innerWriteChildControlRules(Map.Entry<String, Map<String, AbstractControlRule>> item, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        if (item.getValue() == null || item.getValue().size() == 0)
            return;
        for (Map.Entry<String, AbstractControlRule> keyPair : item.getValue().entrySet()) {
            SerializerUtils.writeStartObject(jsonGenerator);
            SerializerUtils.writePropertyName(jsonGenerator,keyPair.getKey());
//            SerializerUtils.writeStartObject(jsonGenerator);
            SerializerUtils.writePropertyValue_Object(jsonGenerator,keyPair.getValue());
//            SerializerUtils.writeEndObject(jsonGenerator);
            SerializerUtils.writeEndObject(jsonGenerator);
        }
    }

    protected void writeSelfInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

    }
}
