/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.entity;

import com.inspur.edp.cef.designtime.api.element.ElementDefaultVauleType;

public class DefaultValueInfo {

    private String defaultValue;
    private String displayDefaultValue;
    private ElementDefaultVauleType defaultVauleType= ElementDefaultVauleType.Vaule;

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDisplayDefaultValue() {
        return displayDefaultValue;
    }

    public void setDisplayDefaultValue(String displayDefaultValue) {
        this.displayDefaultValue = displayDefaultValue;
    }

    public ElementDefaultVauleType getDefaultVauleType() {
        return defaultVauleType;
    }

    public void setDefaultVauleType(ElementDefaultVauleType defaultVauleType) {
        this.defaultVauleType = defaultVauleType;
    }

}
