/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildDtmTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonTriggerPointType;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * The Json Serializer Of CommonDetermination
 *
 * @ClassName: CommonDtmSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDtmSerializer extends CommonOpSerializer {
    @Override
    protected void writeExtendCommonOpBaseProperty(JsonGenerator writer, CommonOperation info) {

    }
    public CommonDtmSerializer(){}
    public CommonDtmSerializer(boolean full){
        super(full);
        isFull = full;
    }
    @Override
    protected void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info) {
        CommonDetermination dtm = (CommonDetermination) info;
        if(isFull||(!dtm.getGetExecutingDataStatus().isEmpty()&&!dtm.getGetExecutingDataStatus().iterator().next().equals(ExecutingDataStatus.None))){
            SerializerUtils.writePropertyName(writer, CefNames.GetExecutingDataStatus);
            writeGetExecutingDataStatus(writer, dtm.getGetExecutingDataStatus());
        }
        if(isFull||(dtm.getTriggerPointType() != CommonTriggerPointType.None)){
            SerializerUtils.writePropertyValue(writer, CefNames.TriggerPointType, dtm.getTriggerPointType().getValue());
        }
        if(isFull||(dtm.getRequestElements()!=null&&dtm.getRequestElements().size()>0))
            writeRequestElements(writer, dtm.getRequestElements());
//        if(isFull||(dtm.getRequestChildElements()!=null&&dtm.getRequestChildElements().size()>0))
//            writeRequestChildElements(writer, dtm.getRequestChildElements());
        if((isFull && dtm.getChildTriggerInfo() !=null) || isSerialChildTriggerInfo(dtm.getChildTriggerInfo()))
            writeChildTriggerInfo(writer, dtm.getChildTriggerInfo());
        if(isFull||(dtm.getRequestChildElements()!=null&&dtm.getRequestChildElements().size()>0))
            writeRequestChildElements(writer, dtm.getRequestChildElements());
    }

    private boolean isSerialChildTriggerInfo(HashMap<String, ChildDtmTriggerInfo> hashMap){
        if(hashMap == null || hashMap.size() == 0)
            return false;
        return true;
    }

    protected void writeRequestElements(JsonGenerator writer, DtmElementCollection childElementsIds) {
        SerializerUtils.writePropertyName(writer, CefNames.RequestElements);

        SerializerUtils.WriteStartArray(writer);

        for (String childElementsId : childElementsIds)
            if(isFull||(childElementsId!=null&&!"".equals(childElementsId)))
                SerializerUtils.writePropertyValue_String(writer, childElementsId);

        SerializerUtils.WriteEndArray(writer);
    }

    protected void writeChildTriggerInfo(JsonGenerator writer, HashMap<String, ChildDtmTriggerInfo> hashMap) {
        SerializerUtils.writePropertyName(writer, CefNames.ChildTriggerInfo);
        SerializerUtils.WriteStartArray(writer);
        if(hashMap != null && hashMap.size() > 0){
            for(Map.Entry<String, ChildDtmTriggerInfo> item: hashMap.entrySet()){
                SerializerUtils.writeStartObject(writer);
                SerializerUtils.writePropertyValue(writer, CefNames.RequestChildCode, item.getKey());
                SerializerUtils.writePropertyName(writer, CefNames.RequestDtmChildElementValue);
                ChildDtmTriggerInfoSerializer dtmTriggerInfoSerializer = new ChildDtmTriggerInfoSerializer(isFull);
                try {
                    dtmTriggerInfoSerializer.serialize(item.getValue(), writer, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                SerializerUtils.writeEndObject(writer);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }

    private void writeDtmElementCollection(JsonGenerator writer, DtmElementCollection childElementsIds) {
        SerializerUtils.WriteStartArray(writer);
        if (childElementsIds.size() > 0) {
            for (String item : childElementsIds) {
                SerializerUtils.writePropertyValue_String(writer, item);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }

    protected void writeRequestChildElements(JsonGenerator writer, HashMap<String, DtmElementCollection> dic) {
        if (dic != null && dic.size() > 0) {
            SerializerUtils.writePropertyName(writer, CefNames.RequestChildElements);
            SerializerUtils.WriteStartArray(writer);
            for (Map.Entry<String, DtmElementCollection> item : dic.entrySet()) {
                SerializerUtils.writeStartObject(writer);
                SerializerUtils.writePropertyValue(writer, CefNames.RequestChildElementKey, item.getKey());
                SerializerUtils.writePropertyName(writer, CefNames.RequestChildElementValue);
                writeDtmElementCollection(writer, item.getValue());
                SerializerUtils.writeEndObject(writer);
            }
            SerializerUtils.WriteEndArray(writer);
        }
    }

}
