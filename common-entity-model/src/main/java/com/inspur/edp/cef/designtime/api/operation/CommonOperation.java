/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.operation;

import com.inspur.edp.cef.designtime.api.util.Guid;

/**
 * The Definition Of Common Operation
 *
 * @ClassName: CommonOperation
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonOperation implements Cloneable {
	public CommonOperation() {
		// 20190529 新增属性IsGenerateComponent,标志操作生成=true,选择构件=false
		setIsGenerateComponent(true);
	}

	/**
	 * 物理主键
	 * 
	 */
	private String privateID;

	public final String getID() {
		return privateID;
	}

	public final void setID(String value) {
		privateID = value;
	}

	/**
	 * 业务主键
	 * 
	 */
	private String privateCode;

	public final String getCode() {
		return privateCode;
	}

	public final void setCode(String value) {
		privateCode = value;
	}

	/**
	 * 显示名称
	 * 
	 */
	private String privateName;

	public final String getName() {
		return privateName;
	}

	public final void setName(String value) {
		privateName = value;
	}

	/**
	 * 描述
	 * 
	 */
	private String privateDescription;

	public final String getDescription() {
		return privateDescription;
	}

	public final void setDescription(String value) {
		privateDescription = value;
	}

	/**
	 * 构件ID
	 * 
	 */
	private String privateComponentId;

	public final String getComponentId() {
		return privateComponentId;
	}

	public final void setComponentId(String value) {
		privateComponentId = value;
	}

	/**
	 * 对应构件实体Name
	 * 
	 */
	private String privateComponentName;

	public final String getComponentName() {
		return privateComponentName;
	}

	public final void setComponentName(String value) {
		privateComponentName = value;
	}

	/**
	 * 构件包名
	 * 
	 */
	private String privateComponentPkgName;

	public final String getComponentPkgName() {
		return privateComponentPkgName;
	}

	public final void setComponentPkgName(String value) {
		privateComponentPkgName = value;
	}

	/**
	 * 是否引用操作
	 * 
	 */
	private boolean privateIsRef;

	public final boolean getIsRef() {
		return privateIsRef;
	}

	public final void setIsRef(boolean value) {
		privateIsRef = value;
	}

	/**
	 * 是否生成构件
	 */
	private boolean privateIsGenerateComponent;

	public final boolean getIsGenerateComponent() {
		return privateIsGenerateComponent;
	}

	public final void setIsGenerateComponent(boolean value) {
		privateIsGenerateComponent = value;
	}

	public CommonOperation clone() {
		try {
			CommonOperation op = (CommonOperation) super.clone();
			op.setID(Guid.newGuid().toString());
			return op;
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
}
