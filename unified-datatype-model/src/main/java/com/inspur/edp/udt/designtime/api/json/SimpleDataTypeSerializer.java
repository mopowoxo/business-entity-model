/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.GspAssociationSerializer;
import com.inspur.edp.cef.designtime.api.json.element.GspEnumValueDeserializer;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.utils.UdtThreadLocal;
import lombok.var;

public class SimpleDataTypeSerializer extends UdtSerializer {


	public SimpleDataTypeSerializer(){
		if(UdtThreadLocal.get()!=null)
			isFull = UdtThreadLocal.get().getfull();
	}
	public SimpleDataTypeSerializer(boolean full){
		super(full);
		isFull = full;
	}

	@Override
	protected void writeUdtExtendInfo(JsonGenerator writer, UnifiedDataTypeDef dataType) {
		SimpleDataTypeDef sUdt = (SimpleDataTypeDef) dataType;
		SerializerUtils.writePropertyValue(writer, UdtNames.DataType, sUdt.getMDataType().toString());//不变
		if(isFull||sUdt.getLength()!=0)
			SerializerUtils.writePropertyValue(writer, UdtNames.Length, sUdt.getLength());
		if(isFull||sUdt.getPrecision()!=0)
			SerializerUtils.writePropertyValue(writer, UdtNames.Precision, sUdt.getPrecision());
		if(isFull||(sUdt.getDefaultValue()!=null&&!"".equals(sUdt.getDefaultValue())))
			SerializerUtils.writePropertyValue(writer, UdtNames.DefaultValue, sUdt.getDefaultValue());
        if(isFull||sUdt.getIsUnique())
        	SerializerUtils.writePropertyValue(writer, UdtNames.IsUnique, sUdt.getIsUnique());
		if(isFull||sUdt.getIsRequired())
			SerializerUtils.writePropertyValue(writer, UdtNames.IsRequired, sUdt.getIsRequired());
		if(isFull||(sUdt.getObjectType()!=null&&sUdt.getObjectType()!=GspElementObjectType.None))
			SerializerUtils.writePropertyValue(writer, UdtNames.ObjectType, sUdt.getObjectType());
        if(isFull||sUdt.isEnableRtrim())
        	SerializerUtils.writePropertyValue(writer, UdtNames.EnableRtrim, sUdt.isEnableRtrim());
		if (this.isFull || sUdt.getEnumIndexType() != null )
			SerializerUtils.writePropertyValue(writer,  UdtNames.EnumIndexType, sUdt.getEnumIndexType());
		if (sUdt.getObjectType().equals(GspElementObjectType.Enum)) {
			writeEnumValueList(writer, sUdt);
		} else if (sUdt.getObjectType().equals(GspElementObjectType.Association)) {
			writeAssociationCollection(writer, sUdt);
		}
	}

	private void writeAssociationCollection(JsonGenerator writer, SimpleDataTypeDef sUdt) {
		if (isFull||(sUdt.getChildAssociations()!=null&&sUdt.getChildAssociations().size() > 0)){
			SerializerUtils.writePropertyName(writer, UdtNames.ChildAssociations);
			SerializerUtils.WriteStartArray(writer);
			if (sUdt.getChildAssociations().size() > 0) {
			GspAssociationSerializer serializer = this.getAssoConvertor();
			for (var asso : sUdt.getChildAssociations()) {
				serializer.serialize(asso, writer, null);
			}
			}
			SerializerUtils.WriteEndArray(writer);
		}
	}

	private GspAssociationSerializer getAssoConvertor() {
		return new GspAssociationSerializer(isFull,new UdtElementSerializer(isFull));
	}

	private void writeEnumValueList(JsonGenerator writer, SimpleDataTypeDef field) {
		if (isFull||(field.getContainEnumValues() != null && field.getContainEnumValues().size() > 0))
		{
			SerializerUtils.writePropertyName(writer, UdtNames.ContainEnumValues);
			SerializerUtils.WriteStartArray(writer);
			if (field.getContainEnumValues() != null && field.getContainEnumValues().size() > 0){
			for (int index = 0; index < field.getContainEnumValues().size(); ++index)
				SerializerUtils.writePropertyValue_Object(writer, field.getContainEnumValues().get(index));}
			SerializerUtils.WriteEndArray(writer);
		}
	}
}
