/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.entity.element;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.entity.commonstructure.CefCommonStructureUtil;
import com.inspur.edp.udt.designtime.api.commonstructure.UdtCommonStructureUtil;
import com.inspur.edp.udt.designtime.api.entity.property.PropertyCollection;
import  java.lang.String;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import  com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import  com.inspur.edp.cef.designtime.api.IGspCommonField;
import  com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;

/**
 UDT字段类型
 
*/
public class UdtElement extends GspCommonField implements Cloneable, IGspCommonField
{
	public UdtElement(PropertyCollection propertys)
	{
		this.propertys = propertys;

	}

	// region 属性
	/** 
	 默认值
	 
	*/

	 @Override
	public String getDefaultValue()
	 {
	 	return (String)(getPropertys().getPropertyName("DefaultValue").getPropertyValue());
	 }
	@Override
	public void setDefaultValue(String value)
	{
		getPropertys().getPropertyName("DefaultValue").setPropertyValue(value);
		super.setDefaultValue(value);
	}

	/** 
	 标签
	 
	*/
	private String labelId;
	@Override
	public String getLabelID()
	{
		if (super.getLabelID()!=null && super.getLabelID().length()==0)
		{
			return getCode();
		}
		return super.getLabelID();
	}
	@Override
	public void setLabelID(String value)
	{
		super.setLabelID(value);
	}

	/** 
	 基础数据类型
	 <see cref="GSPElementDataType"/>
	 
	*/
	 @Override
	 public GspElementDataType getMDataType()
	 {

       return  (GspElementDataType)(getPropertys().getPropertyName("DataType").getPropertyValue());
	 }
	 @Override
	 public void setMDataType(GspElementDataType value)
	 {
		 getPropertys().getPropertyName("DataType").setPropertyValue(value);

	 }

	/** 
	 字段的物理长度
	 <see cref="int"/>
	 
	*/
	 @Override
	 public int getLength()
	 {
	 	return (int)getPropertys().getPropertyName("Length").getPropertyValue();
	 }
	@Override
	public void setLength(int value)
	{
		getPropertys().getPropertyName("Length").setPropertyValue(value);
		super.setLength(value);
	}

	/** 
	 字段精度
	 <see cref="int"/>
	 
	*/
	 @Override
	public int getPrecision()

	 {
	 	return (int) getPropertys().getPropertyName("Precision").getPropertyValue();
	 }
	 @Override
	 public void setPrecision(int value)
	 {
		 getPropertys().getPropertyName("Precision").setPropertyValue(value);

	 }


	/** 
	 是否虚拟
	 <see cref="bool"/>
	 
	*/
	@Override
	public boolean getIsVirtual() {
		return false;
	}
	 @Override
	public void setIsVirtual(boolean value)
	 {

	 	getPropertys().getPropertyName("IsVirtual").setPropertyValue(value);
	 }

	/** 
	 是否必须
	 <see cref="bool"/>
	 
	*/
	 @Override
	 public boolean getIsRequire()
	 {
		 return (boolean)getPropertys().getPropertyName("IsRequire").getPropertyValue();

	 }
	 @Override
	public void setIsRequire(boolean value)
	 {
	 	getPropertys().getPropertyName("IsRequire").setPropertyValue(value);
	 }

	/**
	 *
	 * 是否去掉数据结尾空格
	 */
	@Override
	public boolean isEnableRtrim() {
		return (boolean)getPropertys().getPropertyName("EnableRtrim").getPropertyValue();
	}
	@Override
	public void setEnableRtrim(boolean value) {
		getPropertys().getPropertyName("EnableRtrim").setPropertyValue(value);
	}
	/** 
	 是否多语
	 <see cref="bool"/>
	 
	*/
	 @Override
	public  boolean getIsMultiLanguage(){

		return(boolean) getPropertys().getPropertyName("IsMultiLanguage").getPropertyValue();
	 }
	 @Override
	public void setIsMultiLanguage(boolean value)
	 {

		 getPropertys().getPropertyName("IsMultiLanguage").setPropertyValue(value);
	 }

	/** 
	 是否引用
	 <see cref="bool"/>
	 
	*/
	private boolean privateIsRef;
	@Override
	public boolean getIsRef()
	{
		return privateIsRef;
	}
	@Override
	public void setIsRef(boolean value)
	{
		privateIsRef = value;
	}

	/** 
	 所属对象
	 
	*/
	 public IGspCommonDataType getBelongObject()
	 {
       return super.getBelongObject();

	 }
	 public void setBelongObject(IGspCommonDataType value)
	 {

		 super.setBelongObject(value);
	 }
	/** 
	 对象类型
	 <see cref="GSPElementObjectType"/>
	 
	*/
	 @Override
	 public GspElementObjectType getObjectType()
	 {

	 	return (GspElementObjectType)getPropertys().getPropertyName("ObjectType").getPropertyValue();
	 }
	 @Override
	 public void setObjectType(GspElementObjectType value)
	 {
	 	getPropertys().getPropertyName("ObjectType").setPropertyValue(value);
	 }
	/** 
	 关联信息
	 <see cref="GspAssociationCollection "/>
	 
	*/
	@Override
	public GspAssociationCollection getChildAssociations()
	{
		if ((GspAssociationCollection)getPropertys().getPropertyName("ChildAssociations").getPropertyValue() == null)
		{
			getPropertys().getPropertyName("ChildAssociations").setPropertyValue(new GspAssociationCollection());
		}
		GspAssociationCollection collection = (GspAssociationCollection)getPropertys().getPropertyName("ChildAssociations").getPropertyValue();
		for (GspAssociation item : collection)
		{

			item.setBelongElement(this);
		}
		return collection;
	}
	@Override
	public void setChildAssociations(GspAssociationCollection value) {
		getPropertys().getPropertyName("ChildAssociations").setPropertyValue(value);
	}
		//private set => Propertys["ChildAssociations"].PropertyValue = value;

	/** 
	 包含的枚举列表
	 <see cref="GSPEnumValueCollection"/>
	 
	*/
	@Override
	public GspEnumValueCollection getContainEnumValues()
	{
		if ((GspEnumValueCollection)getPropertys().getPropertyName("ContainEnumValues").getPropertyValue() == null)
		{
			getPropertys().getPropertyName("ContainEnumValues").setPropertyValue(new GspEnumValueCollection());
		}
		return (GspEnumValueCollection)getPropertys().getPropertyName("ContainEnumValues").getPropertyValue();
	}

	@Override
	public void setContainEnumValues(GspEnumValueCollection value) {
		getPropertys().getPropertyName("ContainEnumValues").setPropertyValue(value);
	}

	//get => enumValueCollection ?? (enumValueCollection = new EnumValueCollection());
		//private set => Propertys["ContainEnumValues"].PropertyValue = value;

	public EnumIndexType getEnumIndexType(){
		if(getPropertys().getPropertyName("EnumIndexType")==null) {

			return EnumIndexType.Integer;
		}
		return (EnumIndexType) getPropertys().getPropertyName("EnumIndexType").getPropertyValue();
	}


	public void setEnumIndexType(EnumIndexType indexType){
		getPropertys().getPropertyName("EnumIndexType").setPropertyValue(indexType);
	}
	/** 
	 是否启用业务字段
	 
	*/
	@Override
	public boolean getIsUdt()
	{
		if (getPropertys().contains("IsUdt"))
		{
			return (boolean)getPropertys().getPropertyName("IsUdt").getPropertyValue();
		}
		return false;
	}
	 @Override
	 public void setIsUdt(boolean value)
	 {
		 getPropertys().getPropertyName("IsUdt").setPropertyValue(value);

	 }

	/** 
	 业务字段包名
	 
	*/
	 @Override
	 public String getUdtPkgName()
	 {
		return (String) getPropertys().getPropertyName("UdtPkgName").getPropertyValue();
	 }

	 @Override
	 public void setUdtPkgName(String value)
	 {
		 getPropertys().getPropertyName("UdtPkgName").setPropertyValue(value);

	 }

	/** 
	 业务字段ID
	 
	*/
	 @Override
	public  String getUdtID()
	 {
	 	return (String)getPropertys().getPropertyName("UdtID").getPropertyValue();
	 }
	 @Override
	 public void setUdtID(String value)
	 {
		 getPropertys().getPropertyName("UdtID").setPropertyValue(value);

	 }

	/** 
	 引用udt
	 
	*/
	private UnifiedDataTypeDef privateUnifiedDataType;
	public final UnifiedDataTypeDef getUnifiedDataType()
	{
		return privateUnifiedDataType;
	}
	public final void setUnifiedDataType(UnifiedDataTypeDef value)
	{
		privateUnifiedDataType = value;
	}

	private PropertyCollection propertys;
	/** 
	 属性集合
	 
	*/
	public final PropertyCollection getPropertys()
	{
		if (propertys == null)
		{
			propertys = new PropertyCollection();
		}
		return propertys;
	}

	// endregion

	// region 方法

	@JsonIgnore
	public String getEnumTypeName(){
		return getLabelID() + "Enum";
	}

	/** 
	 克隆方法
	 
	 @return <see cref="object"/>
	*/
	@Override
	public  UdtElement clone()
	{
		Object tempVar = null;
		try {
			tempVar = super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return  (UdtElement)((tempVar instanceof UdtElement) ? tempVar : null);
	}
	// endregion

	@Override
	protected CefCommonStructureUtil getCefCommonStructureUtil(){
		return UdtCommonStructureUtil.getInstance();
	}
}
