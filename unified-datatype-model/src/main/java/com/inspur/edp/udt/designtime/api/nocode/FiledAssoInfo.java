/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.nocode;

import java.util.List;

public class FiledAssoInfo {
    public String getRefModelID() {
        return refModelID;
    }

    public void setRefModelID(String refModelID) {
        this.refModelID = refModelID;
    }

    public String getRefObjectID() {
        return refObjectID;
    }

    public void setRefObjectID(String refObjectID) {
        this.refObjectID = refObjectID;
    }

    public List<RefField> getElements() {
        return elements;
    }

    public void setElements(List<RefField> elements) {
        this.elements = elements;
    }

    private String refModelID;
    private String refObjectID;
    private List<RefField> elements;
}
