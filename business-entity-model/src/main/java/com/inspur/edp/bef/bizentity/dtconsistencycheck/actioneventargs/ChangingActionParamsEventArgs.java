/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs;

import java.util.List;

public class ChangingActionParamsEventArgs extends AbstractMgrActionEventArgs {

  protected List<String> newParameters;
  protected List<String> originalParameters;

  public ChangingActionParamsEventArgs() {

  }

  public ChangingActionParamsEventArgs(String beId, String actionId, String metadataPath,
      List<String> newParameters, List<String> originalParameters) {
    super(beId, actionId, metadataPath);
    this.newParameters = newParameters;
    this.originalParameters = originalParameters;
  }

  public List<String> getNewParameters() {
    return newParameters;
  }

  public List<String> getOriginalParameters() {
    return originalParameters;
  }

  public void setNewParameters(List<String> newParameters) {
    this.newParameters = newParameters;
  }

  public void setOriginalParameters(List<String> originalParameters) {
    this.originalParameters = originalParameters;
  }
}
