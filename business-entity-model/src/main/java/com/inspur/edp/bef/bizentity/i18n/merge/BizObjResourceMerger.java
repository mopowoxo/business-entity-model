/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.i18n.merge;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonElementResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonobjectResourceMerger;

public class BizObjResourceMerger extends CommonobjectResourceMerger {

  public BizObjResourceMerger(GspBizEntityObject object, ICefResourceMergeContext context) {
    super(object, context);
  }

  @Override
  protected void extractExtendObjProperties(IGspCommonObject dataType) {

  }

  @Override
  protected CommonElementResourceMerger getCommonEleResourceMerger(ICefResourceMergeContext context,
      IGspCommonElement element) {
    return new BizEleResourceMerger(
        (GspBizEntityElement) ((element instanceof GspBizEntityElement) ? element : null), context);

  }

  @Override
  protected CommonobjectResourceMerger getObjectResourceMerger(ICefResourceMergeContext context,
      IGspCommonObject obj) {
    return new BizObjResourceMerger(
        (GspBizEntityObject) ((obj instanceof GspBizEntityObject) ? obj : null), context);

  }
}
