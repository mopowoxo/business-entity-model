/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.common;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizAction;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.internalbeaction.IInternalBEAction;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.IInternalMgrAction;
import com.inspur.edp.cef.designtime.api.util.DataValidator;
import java.util.ArrayList;

/**
 * be扩展方法
 */
public final class GspBusinessEntityExtend {

  /**
   * 获取不包含内置动作的Manager动作集合
   */
  public static ArrayList<BizMgrAction> getCustomMgrActions(GspBusinessEntity be) {
    DataValidator.checkForNullReference(be, "be");
    ArrayList<BizMgrAction> result = new ArrayList<BizMgrAction>();
    for (BizOperation op : be.getBizMgrActions()) {
      BizMgrAction action = (BizMgrAction) op;
      if (action instanceof IInternalMgrAction) {
        continue;
      }
      result.add(action);
    }
    return result;
  }

  /**
   * 获取不包含内置动作的BE动作集合
   */
  public static Iterable<BizAction> getCustomBEActions(GspBizEntityObject obj) {
    DataValidator.checkForNullReference(obj, "obj");
    ArrayList<BizAction> result = new ArrayList<BizAction>();
    for (BizOperation op : obj.getBizActions()) {
      BizAction action = (BizAction) op;
      if (action instanceof IInternalBEAction) {
        continue;
      }
      result.add(action);
    }
    return result;
  }
}
