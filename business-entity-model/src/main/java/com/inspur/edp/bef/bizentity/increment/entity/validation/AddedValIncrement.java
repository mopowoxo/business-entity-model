/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.increment.entity.validation;

import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

public class AddedValIncrement extends ValIncrement {

  private Validation action;

  @Override
  public IncrementType getIncrementType() {
    return IncrementType.Added;
  }

  public Validation getAction() {
    return action;
  }

  public void setAction(Validation action) {
    this.action = action;
  }
}
