/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation.internalmgraction;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.operation.TccAction;

public class TccSettingElement {

  /**
   * 默认构造函数
   */
  public TccSettingElement() {
  }

  /**
   * TCC 字段ID
   */
  private String privateID;

  public final String getID() {
    return privateID;
  }

  public final void setID(String value) {
    privateID = value;
  }

  /**
   * TCC 字段编号
   */
  private String privateCode;

  public final String getCode() {
    return privateCode;
  }

  public final void setCode(String value) {
    privateCode = value;
  }

  /**
   * TCC 字段名称
   */
  private String privateName;

  public final String getName() {
    return privateName;
  }

  public final void setName(String value) {
    privateName = value;
  }

  /**
   * 触发字段
   */
  private java.util.ArrayList<String> privateTriggerFields;

  public final java.util.ArrayList<String> getTriggerFields() {
    return privateTriggerFields;
  }

  public final void setTriggerFields(java.util.ArrayList<String> value) {
    privateTriggerFields = value;
  }

  private TccAction privateTccAction;

  public final TccAction getTccAction() {
    return privateTccAction;
  }

  public final void setTccAction(TccAction action) {
    privateTccAction = action;
  }

  /**
   * 所在Node
   */
  private GspBizEntityObject privateOwner;

  public final GspBizEntityObject getOwner() {
    return privateOwner;
  }

  public final void setOwner(GspBizEntityObject value) {
    privateOwner = value;
  }
}
