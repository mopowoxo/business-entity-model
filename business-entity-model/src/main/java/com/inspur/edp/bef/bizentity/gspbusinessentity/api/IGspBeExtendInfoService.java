/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.gspbusinessentity.api;

import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfo;
import java.util.List;

/**
 *BE扩展信息服务
 *
 * @author hanll02
 */
public interface IGspBeExtendInfoService {

  /**
   * 根据BE元数据ID获取指定的BE扩展信息
   *
   * @param id BE元数据id
   * @return 指定的BE扩展信息
   */
  GspBeExtendInfo getBeExtendInfo(String id);

  /**
   * 根据BE元数据的configId信息获取指定的BE扩展信息
   *
   * @param configId BE元数据的configId
   * @return 指定的BE扩展信息
   */
  GspBeExtendInfo getBeExtendInfoByConfigId(String configId);

  /**
   * 获取所有的BE扩展信息
   *
   * @return
   */
  List<GspBeExtendInfo> getBeExtendInfos();

  /**
   * 保存BE扩展信息
   *
   * @param infos
   */
  void saveGspBeExtendInfos(List<GspBeExtendInfo> infos);

  /**
   * 删除指定BE元数据id对应的的扩展信息
   *
   * @param id BE元数据id
   */
  void deleteBeExtendInfo(String id);
}
