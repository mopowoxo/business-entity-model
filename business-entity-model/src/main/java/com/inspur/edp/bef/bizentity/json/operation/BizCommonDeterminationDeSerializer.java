/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizCommonDetermination;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmDeserializer;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;

public class BizCommonDeterminationDeSerializer extends CommonDtmDeserializer {

  protected CommonOperation CreateCommonOp() {
    return new BizCommonDetermination();
  }

  @Override
  protected boolean readExtendDtmProp(CommonOperation op, String propName, JsonParser jsonParser) {
    boolean result = false;
    BizCommonDetermination dtm = (BizCommonDetermination) op;
    switch (propName) {
      case BizEntityJsonConst.Parameters:
        result = true;
        readParameters(jsonParser, (BizCommonDetermination) dtm);
        break;
      case BizEntityJsonConst.RunOnce: {
        result = true;
        dtm.setRunOnce(SerializerUtils.readPropertyValue_boolean(jsonParser));
        break;
      }
    }
    return result;
  }

  private void readParameters(JsonParser jsonParser, BizCommonDetermination action) {
    BizParaDeserializer paraDeserializer = new BizActionParaDeserializer();
    BizParameterCollection<BizParameter> collection = new BizParameterCollection<>();
    SerializerUtils.readArray(jsonParser, paraDeserializer, collection);
    action.setParameterCollection(collection);
  }
}
