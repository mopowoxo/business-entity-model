/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.i18n.merge;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonElementResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.GspAssoResourceMerger;

public class BizEleResourceMerger extends CommonElementResourceMerger {

  public BizEleResourceMerger(GspBizEntityElement field, ICefResourceMergeContext context) {
    super(field, context);
  }

  @Override
  protected void extractExtendElementProperties(IGspCommonElement iGspCommonElement) {

  }

  @Override
  protected GspAssoResourceMerger getGSPAssoResourceMerger(ICefResourceMergeContext context,
      GspAssociation asso) {
    return new BizAssoResourceMerger(asso, getContext());

  }
}
