/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.model;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.beenum.BECategory;
import com.inspur.edp.bef.bizentity.beenum.GspDataLockType;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.common.InternalActionUtil;
import com.inspur.edp.bef.bizentity.json.object.BizObjectDeserializer;
import com.inspur.edp.bef.bizentity.json.operation.BizMgrActionCollectionDeserializer;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.json.model.CommonModelDeserializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectDeserializer;

public class BizEntityDeserializer extends CommonModelDeserializer {


  @Override
  protected void beforeCMModelDeserializer(GspCommonModel cm) {
    GspBusinessEntity be = (GspBusinessEntity) cm;
    be.setDependentEntityId("");
    be.setDependentEntityName("");
    be.setDependentEntityPackageName("");

  }

  @Override
  protected boolean readExtendModelProperty(GspCommonModel cm, String propName, JsonParser reader) {
    GspBusinessEntity be = (GspBusinessEntity) cm;
    boolean hasProperty = true;
    switch (propName) {
      case BizEntityJsonConst.Category:
        be.setCategory(
            SerializerUtils.readPropertyValue_Enum(reader, BECategory.class, BECategory.values()));
        break;
      case BizEntityJsonConst.DataLockType:
        be.setDataLockType(SerializerUtils
            .readPropertyValue_Enum(reader, GspDataLockType.class, GspDataLockType.values()));
        break;
      case BizEntityJsonConst.ExtendType:
        SerializerUtils.readPropertyValue_String(reader);
        break;
      case BizEntityJsonConst.DependentEntityId:
        be.setDependentEntityId(SerializerUtils.readPropertyValue_String(reader));
        break;
      case BizEntityJsonConst.DependentEntityName:
        be.setDependentEntityName(SerializerUtils.readPropertyValue_String(reader));
        break;
      case BizEntityJsonConst.CacheConfiguration:
        be.setCacheConfiguration(SerializerUtils.readPropertyValue_String(reader));
        break;
      case BizEntityJsonConst.DependentEntityPackageName:
        be.setDependentEntityPackageName(SerializerUtils.readPropertyValue_String(reader));
        break;
      case BizEntityJsonConst.EnableCaching:
        be.setEnableCaching(SerializerUtils.readPropertyValue_boolean(reader));
        break;
      case BizEntityJsonConst.EnableTreeDtm:
        be.setEnableTreeDtm(SerializerUtils.readPropertyValue_boolean(reader));
        break;
      case BizEntityJsonConst.IsUsingTimeStamp:
        be.setIsUsingTimeStamp(SerializerUtils.readPropertyValue_boolean(reader));
        break;
      case BizEntityJsonConst.BizMgrActions:
        readBizMgrActions(reader, be);
        break;
      case BizEntityJsonConst.ComponentAssemblyName:
        be.setComponentAssemblyName(SerializerUtils.readPropertyValue_String(reader));
        break;
      case BizEntityJsonConst.AssemblyName:
        SerializerUtils.readPropertyValue_String(reader);
        break;
      case BizEntityJsonConst.Authorizations:
        readAuthorizations(reader, be);
        break;
      case BizEntityJsonConst.EnableApproval:
        be.setEnableApproval(SerializerUtils.readPropertyValue_boolean(reader));
        break;
      case BizEntityJsonConst.TccSupported:
        be.setTccSupported(SerializerUtils.readPropertyValue_boolean(reader));
        break;
      case BizEntityJsonConst.AutoTccLock:
        be.setAutoTccLock(SerializerUtils.readPropertyValue_boolean(reader));
        break;
      case BizEntityJsonConst.AutoComplete:
        be.setAutoComplete(SerializerUtils.readPropertyValue_boolean(reader));
        break;
      case BizEntityJsonConst.AutoCancel:
        be.setAutoCancel(SerializerUtils.readPropertyValue_boolean(reader));
        break;
      default:
        hasProperty = false;
        break;
    }
    return hasProperty;
  }

  private void readAuthorizations(JsonParser reader, GspBusinessEntity be) {
    //未序列化
    SerializerUtils.readStartArray(reader);
    SerializerUtils.readEndArray(reader);
  }

  private void readBizMgrActions(JsonParser jsonParser, GspBusinessEntity be) {
    BizMgrActionCollectionDeserializer deserializer = new BizMgrActionCollectionDeserializer();
    BizMgrActionCollection actionCollection = deserializer.deserialize(jsonParser, null);
    for (BizOperation action : actionCollection) {
      if (!InternalActionUtil.InternalMgrActionIDs.contains(action.getID())) {
        be.getBizMgrActions().add(action);
      }
    }
  }


  @Override
  protected GspCommonModel createCommonModel() {
    return new GspBusinessEntity();
  }

  @Override
  protected CmObjectDeserializer createCmObjectDeserializer() {
    return new BizObjectDeserializer();
  }
}
