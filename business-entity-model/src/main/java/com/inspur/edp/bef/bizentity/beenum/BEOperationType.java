/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.beenum;

/**
 * BE 操作类型
 */
public enum BEOperationType {
  /**
   * 自定义操作
   */
  BizMgrAction,

  /**
   * 实体操作
   */
  BizAction,

  /**
   * 校验规则
   */
  Validation,

  /**
   * 联动计算
   */
  Determination,

  /**
   * 查询
   */
  Query,

  /**
   * 事件订阅
   */
  Subscription,

  /**
   * 初始化
   */
  Initiation,

  /**
   * 系统服务
   */
  BasicService,
  /**
   * Tcc动作
   */
  TccAction;

  public int getValue() {
    return this.ordinal();
  }

  public static BEOperationType forValue(int value) {
    return values()[value];
  }
}
