/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmEntityControlRuleDefParser;

public class BeEntityControlRuleDefParser extends
    CmEntityControlRuleDefParser<BeEntityControlRuleDef> {

  @Override
  protected final CmEntityControlRuleDef createCmEntityRuleDef() {
    return new BeEntityControlRuleDef(null);
  }

  protected JsonDeserializer getChildDeserializer(String childTypeName) {
    return new BeFieldControlRuleDefParser();
  }

}
