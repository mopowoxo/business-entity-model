/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs;

public class ChangingActionReturnEventArgs extends AbstractMgrActionEventArgs {

  protected String newReturnInfo;
  protected String orignalReturnInfo;

  public ChangingActionReturnEventArgs() {

  }

  public ChangingActionReturnEventArgs(String beId, String actionId, String metadataPath,
      String newReturnInfo, String orignalReturnInfo) {
    super(beId, actionId, metadataPath);
    this.newReturnInfo = newReturnInfo;
    this.orignalReturnInfo = orignalReturnInfo;
  }

  public String getNewReturnInfo() {
    return newReturnInfo;
  }

  public void setNewReturnInfo(String newReturnInfo) {
    this.newReturnInfo = newReturnInfo;
  }

  public String getOrignalReturnInfo() {
    return orignalReturnInfo;
  }

  public void setOrignalReturnInfo(String orignalReturnInfo) {
    this.orignalReturnInfo = orignalReturnInfo;
  }
}
