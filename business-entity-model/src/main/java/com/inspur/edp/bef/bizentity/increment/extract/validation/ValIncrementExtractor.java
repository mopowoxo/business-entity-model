/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.increment.extract.validation;

import com.inspur.edp.bef.bizentity.increment.entity.validation.ValIncrement;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;

public class ValIncrementExtractor extends AbstractIncrementExtractor {

  public ValIncrement extractorIncrement(Validation oldAction, Validation newAction,
      CommonDataTypeControlRule rule, CommonDataTypeControlRuleDef def) {

    if (oldAction == null && newAction == null) {
      return null;
    } else if (oldAction == null) {
      return new AddedValExtractor().extract(newAction);
    } else if (newAction == null) {
      //TODO 删除
      return null;
    } else {
      //TODO 修改
      return null;
    }
  }
}
