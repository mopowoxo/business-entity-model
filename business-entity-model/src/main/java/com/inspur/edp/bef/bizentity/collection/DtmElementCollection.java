/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.collection;

import com.inspur.edp.cef.designtime.api.collection.BaseList;
import java.util.ArrayList;

/**
 * 联动规则过滤字段集合
 */
public class DtmElementCollection extends BaseList<String> implements Cloneable {

  private static final long serialVersionUID = 1L;

  /**
   * 是否包含指定字段ID集合中的任意字段
   *
   * @param elementLabelIDs 指定的字段LabelID集合
   */
  public final boolean ContainsAny(ArrayList<String> elementLabelIDs) {
    if (this.size() == 0) {
      return true;
    }
    if (elementLabelIDs == null || elementLabelIDs.size() == 0) {
      return false;
    }
    for (String var : elementLabelIDs) {
      if (!this.contains(var)) {
        return false;
      }
    }
    return true;
  }
}
