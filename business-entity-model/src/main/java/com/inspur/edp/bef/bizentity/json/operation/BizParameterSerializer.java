/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

public class BizParameterSerializer<T extends BizParameter> extends JsonSerializer<T> {

  protected boolean isFull = true;

  public BizParameterSerializer() {
  }

  public BizParameterSerializer(boolean full) {
    isFull = full;
  }

  @Override
  public void serialize(BizParameter value, JsonGenerator gen, SerializerProvider serializers) {

    WriteOperationInfo(gen, value);
  }

  private void WriteOperationInfo(JsonGenerator writer, BizParameter para) {
    //{
    SerializerUtils.writeStartObject(writer);
    SerializerUtils.writePropertyValue(writer, CommonModelNames.ID, para.getID());
    if (isFull || para.getParamCode() != null && !"".equals(para.getParamCode())) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ParamCode, para.getParamCode());
    }
    if (isFull || para.getParamName() != null && !"".equals(para.getParamName())) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ParamName, para.getParamName());
    }
    SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ParameterType,
        para.getParameterType().getValue());
    if (isFull || para.getCollectionParameterType().getValue() != 0) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.CollectionParameterType,
          para.getCollectionParameterType().getValue());
    }
    SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Assembly, para.getAssembly());
    SerializerUtils
        .writePropertyValue(writer, BizEntityJsonConst.ClassName, para.getNetClassName());
//        if(isFull||para.getClassName()!=null&&!"".equals(para.getClassName()))
    SerializerUtils
        .writePropertyValue(writer, BizEntityJsonConst.JavaClassName, para.getClassName());
    if (isFull || para.getMode().getValue() != 0) {
      SerializerUtils
          .writePropertyValue(writer, BizEntityJsonConst.Mode, para.getMode().getValue());
    }
    if (isFull || para.getParamDescription() != null && !"".equals(para.getParamDescription())) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ParamDescription,
          para.getParamDescription());
    }
    if (isFull || para.getActualValue() != null) {
      SerializerUtils
          .writePropertyValue(writer, BizEntityJsonConst.ParamActualValue, para.getActualValue());
    }

    SerializerUtils.writeEndObject(writer);
  }
}
