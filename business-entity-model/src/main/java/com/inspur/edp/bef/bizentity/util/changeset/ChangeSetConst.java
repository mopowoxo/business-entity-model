/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.util.changeset;

public class ChangeSetConst {

  public static final String metadataInfo = "metadataInfo";
  public static final String relatedMetadatas = "relatedMetadatas";
  public static final String actionChangeSet = "actionChangeSets";
  public static final String changeDetail = "changeDetail";
  public static final String changeType = "changeType";
  public static final String isNecessary = "isNecessary";
  public static final String changeInfo = "changeInfo";

  //action
  public static final String actionId = "actionId";
  public static final String actionCode = "actionCode";
  //    public static final String actionName = "actionName";
  public static final String mgrAction = "mgrAction";
  public static final String parameterId = "parameterId";
  public static final String parameterCode = "parameterCode";

  // object
  public static final String objectChangeSets = "objectChangeSets";
  public static final String bizObjectId = "bizObjectId";
  public static final String bizObjectCode = "bizObjectCode";
  public static final String bizObject = "bizObject";
  public static final String parentObjIDElementId = "parentObjIDElementId";

  // element
  public static final String elementChangeSets = "elementChangeSets";
  public static final String bizElementId = "bizElementId";
  public static final String bizElementCode = "bizElementCode";
  public static final String bizElement = "bizElement";


}
