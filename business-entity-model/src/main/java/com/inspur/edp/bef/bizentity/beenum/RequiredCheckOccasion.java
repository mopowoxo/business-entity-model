/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.beenum;

/**
 * 必填的检查时机
 */
public enum RequiredCheckOccasion {
  /**
   * 仅保存时
   */
  Save(1),

  /**
   * 此选项在设计器上不暴露, 不应该存在修改时验证保存时不执行的情况
   */
  Modify(2),

  /**
   * 所有时机 二进制为全1
   */
  All(-1);

  private final int intValue;
  private static java.util.HashMap<Integer, RequiredCheckOccasion> mappings;

  private synchronized static java.util.HashMap<Integer, RequiredCheckOccasion> getMappings() {
    if (mappings == null) {
      mappings = new java.util.HashMap<Integer, RequiredCheckOccasion>();
    }
    return mappings;
  }

  RequiredCheckOccasion(int value) {
    intValue = value;
    RequiredCheckOccasion.getMappings().put(value, this);
  }

  public int getValue() {
    return intValue;
  }

  public static RequiredCheckOccasion forValue(int value) {
    return getMappings().get(value);
  }
}
