/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.increment.extract;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.increment.BizObjectIncrement;
import com.inspur.edp.bef.bizentity.increment.extract.determination.DtmIncrementExtractor;
import com.inspur.edp.bef.bizentity.increment.extract.validation.ValIncrementExtractor;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.bizentity.operation.collection.DeterminationCollection;
import com.inspur.edp.bef.bizentity.operation.collection.ValidationCollection;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.das.commonmodel.entity.object.increment.extract.CommonObjectIncrementExtractor;
import com.inspur.edp.das.commonmodel.entity.object.increment.extract.ModifyCommonObjectExtractor;
import lombok.var;

public class ModifyBizObjectExtractor extends ModifyCommonObjectExtractor {

  public ModifyBizObjectExtractor() {
    super();
  }

  public ModifyBizObjectExtractor(boolean includeAll) {
    super(includeAll);
  }

  @Override
  protected CommonObjectIncrementExtractor createChildExtractor() {
    return new BizObjectIncrementExtractor(this.includeAll);
  }

  @Override
  protected void extractExtendObjectIncrement(ModifyEntityIncrement increment,
      GspCommonDataType oldDataType, GspCommonDataType newDataType, CommonDataTypeControlRule rule,
      CommonDataTypeControlRuleDef def) {
    extractDeterminations((BizObjectIncrement) increment, (GspBizEntityObject) oldDataType,
        (GspBizEntityObject) newDataType, rule, def);
    extractValidations((BizObjectIncrement) increment, (GspBizEntityObject) oldDataType,
        (GspBizEntityObject) newDataType, rule, def);
  }


  private void extractDeterminations(BizObjectIncrement increment, GspBizEntityObject oldObj,
      GspBizEntityObject newObj, CommonDataTypeControlRule rule, CommonDataTypeControlRuleDef def) {

    DeterminationCollection oldActions = oldObj.getDeterminations();
    DeterminationCollection newActions = newObj.getDeterminations();
    if (oldActions.size() < 1 && newActions.size() < 1) {
      return;
    }

    if (newActions.size() < 1) {
      //TODO 暂不支持删除动作
      return;
    }
    var extractor = new DtmIncrementExtractor();
    for (BizOperation action : newActions) {
      Determination oldAction = (Determination) oldActions.getItem(action.getID());

      var actionIncrement = extractor
          .extractorIncrement(oldAction, (Determination) action, rule, def);
      if (actionIncrement == null) {
        continue;
      }
      increment.getDeterminations().put(action.getID(), actionIncrement);
    }
  }

  private void extractValidations(BizObjectIncrement increment, GspBizEntityObject oldObj,
      GspBizEntityObject newObj, CommonDataTypeControlRule rule, CommonDataTypeControlRuleDef def) {

    ValidationCollection oldActions = oldObj.getValidations();
    ValidationCollection newActions = newObj.getValidations();
    if (oldActions.size() < 1 && newActions.size() < 1) {
      return;
    }

    if (newActions.size() < 1) {
      //TODO 暂不支持删除动作
      return;
    }
    var extractor = new ValIncrementExtractor();
    for (BizOperation action : newActions) {
      Validation oldAction = (Validation) oldActions.getItem(action.getID());

      var actionIncrement = extractor.extractorIncrement(oldAction, (Validation) action, rule, def);
      if (actionIncrement == null) {
        continue;
      }
      increment.getValidations().put(action.getID(), actionIncrement);
    }
  }

  protected ModifyEntityIncrement createModifyEntityIncrement() {
    return new BizObjectIncrement();
  }
}
