/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.beenum.BEDeterminationType;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.beenum.RequestNodeTriggerType;
import com.inspur.edp.bef.bizentity.collection.DtmElementCollection;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.util.HashMap;
import java.util.Map;

public class BizDeterminationSerializer extends BizOperationSerializer<Determination> {

  public BizDeterminationSerializer() {
  }

  public BizDeterminationSerializer(boolean full) {
    super(full);
    isFull = full;
  }

  @Override
  protected void writeExtendOperationBaseProperty(JsonGenerator writer, BizOperation op) {

  }

  @Override
  protected void writeExtendOperationSelfProperty(JsonGenerator writer, BizOperation op) {
    Determination dtm = (Determination) op;
    if (isFull || (dtm.getDeterminationType() != BEDeterminationType.Transient)) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DeterminationType,
          dtm.getDeterminationType().getValue());
    }
    if (isFull || dtm.getRunOnce()) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.RunOnce, dtm.getRunOnce());
    }
    int triggerTimePointType = 0;
    for (BETriggerTimePointType timePointType : dtm.getTriggerTimePointType()) {
      triggerTimePointType += timePointType.getValue();
    }
    SerializerUtils
        .writePropertyValue(writer, BizEntityJsonConst.TriggerTimePointType, triggerTimePointType);

    int requestNodeTriggerType = 0;
    for (RequestNodeTriggerType requestNodeTrigger : dtm.getRequestNodeTriggerType()) {
      requestNodeTriggerType += requestNodeTrigger.getValue();
    }
    SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.RequestNodeTriggerType,
        requestNodeTriggerType);

    writeRequestElements(writer, dtm);
    writeParameters(writer, dtm);
    writeRequestChildElements(writer, dtm);
  }

  private void writeParameters(JsonGenerator writer, Determination determination) {
    if (isFull || (determination.getParameterCollection() != null
        && determination.getParameterCollection().getCount() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Parameters);
      //[
      SerializerUtils.WriteStartArray(writer);
      if (determination.getParameterCollection() != null
          && determination.getParameterCollection().getCount() > 0) {
        for (Object item : determination.getParameterCollection()) {
          new BizParameterSerializer(isFull).serialize((BizParameter) item, writer, null);
        }
      }
      //]
      SerializerUtils.WriteEndArray(writer);
    }
  }

  private void writeRequestElements(JsonGenerator writer, Determination dtm) {
    if (isFull || (dtm.getRequestElements() != null && dtm.getRequestElements().size() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.RequestElements);
      writeDtmElementCollection(writer, dtm.getRequestElements());
    }
  }

  private void writeDtmElementCollection(JsonGenerator writer,
      DtmElementCollection childElementsIds) {
    //[
    SerializerUtils.WriteStartArray(writer);
    if (childElementsIds.size() > 0) {
      for (String item : childElementsIds) {
        SerializerUtils.writePropertyValue_String(writer, item);
      }
    }
    //]
    SerializerUtils.WriteEndArray(writer);
  }

  private void writeRequestChildElements(JsonGenerator writer, Determination dtm) {
    HashMap<String, DtmElementCollection> dic = dtm.getRequestChildElements();
    if (isFull || (dic != null && dic.size() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.RequestChildElements);
      SerializerUtils.WriteStartArray(writer);
      for (Map.Entry<String, DtmElementCollection> item : dic.entrySet()) {
        SerializerUtils.writeStartObject(writer);
        SerializerUtils
            .writePropertyValue(writer, BizEntityJsonConst.RequestChildElementKey, item.getKey());
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.RequestChildElementValue);
        writeDtmElementCollection(writer, item.getValue());
        SerializerUtils.writeEndObject(writer);
      }
      SerializerUtils.WriteEndArray(writer);
    }
  }
}
