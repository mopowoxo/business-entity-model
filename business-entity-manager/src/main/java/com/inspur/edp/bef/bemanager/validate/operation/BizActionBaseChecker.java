/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.validate.operation;

import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.isLegality;

import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import org.codehaus.plexus.util.StringUtils;


public abstract class BizActionBaseChecker extends BizOperationChecker {

  //BE参数校验
  private void checkParameters(BizActionBase para) {
    for (Object par : para.getParameters()) {
      par = par instanceof IBizParameter ? (IBizParameter) par : null;
      if (par == null) {
        return;
      }
      if (StringUtils.isEmpty(((IBizParameter) par).getParamCode())) {
        throw new RuntimeException(
            "方法[" + para.getCode() + "]的参数[" + ((IBizParameter) par).getParamName() + "]编号为空，请修改！");
      }
      if (isLegality(((IBizParameter) par).getParamCode())) {
        continue;
      }
      opException("方法[" + para.getCode() + "]的参数[" + ((IBizParameter) par).getParamCode()
          + "]是Java关键字，请修改！");
    }
  }

  protected final void checkBizActionBase(BizActionBase actionBase) {
    checkBizOperation(actionBase);
    checkParameters(actionBase);
    checkBizActionExtented(actionBase);
  }

  protected void checkBizActionExtented(BizActionBase actionBase) {
  }
}
