/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.service.cdmparser;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;

public class CdmColumnInfo {

  public CdmColumnInfo() {
  }

  private String columnId;

  @JsonProperty("ColumnId")
  public final String getColumnId() {
    return columnId;
  }

  public final void setColumnId(String value) {
    columnId = value;
  }

  private String objectID;

  @JsonProperty("ObjectID")
  public final String getObjectID() {
    return objectID;
  }

  public final void setObjectID(String value) {
    objectID = value;
  }

  private String name;

  @JsonProperty("Name")
  public final String getName() {
    return name;
  }

  public final void setName(String value) {
    name = value;
  }

  private String code;

  @JsonProperty("Code")
  public final String getCode() {
    return code;
  }

  public final void setCode(String value) {
    code = value;
  }

  private int creationDate;

  @JsonProperty("CreationDate")
  public final int getCreationDate() {
    return creationDate;
  }

  public final void setCreationDate(int value) {
    creationDate = value;
  }

  private String creator;

  @JsonProperty("Creator")
  public final String getCreator() {
    return creator;
  }

  public final void setCreator(String value) {
    creator = value;
  }

  private int modificationDate;

  @JsonProperty("ModificationDate")
  public final int getModificationDate() {
    return modificationDate;
  }

  public final void setModificationDate(int value) {
    modificationDate = value;
  }

  private String modifier;

  @JsonProperty("Modifier")
  public final String getModifier() {
    return modifier;
  }

  public final void setModifier(String value) {
    modifier = value;
  }

  private String comment;

  @JsonProperty("Comment")
  public final String getComment() {
    return comment;
  }

  public final void setComment(String value) {
    comment = value;
  }

  private GspElementDataType dataType;

  @JsonProperty("DataType")
  public final GspElementDataType getDataType() {
    return dataType;
  }

  public final void setDataType(GspElementDataType value) {
    dataType = value;
  }

  private String length;

  @JsonProperty("Length")
  public final String getLength() {
    return length;
  }

  public final void setLength(String value) {
    length = value;
  }

  //是否自增量
  private boolean identity;

  @JsonProperty("Identity")
  public final boolean getIdentity() {
    return identity;
  }

  public final void setIdentity(boolean value) {
    identity = value;
  }

  private boolean mandatory;

  //禁止为空
  @JsonProperty("Mandatory")
  public final boolean getMandatory() {
    return mandatory;
  }

  public final void setMandatory(boolean value) {
    mandatory = value;
  }

  private String extendedAttributesText;

  //扩展属性
  @JsonProperty("ExtendedAttributesText")
  public final String getExtendedAttributesText() {
    return extendedAttributesText;
  }

  public final void setExtendedAttributesText(String value) {
    extendedAttributesText = value;
  }

  private String physicalOptions;

  @JsonProperty("PhysicalOptions")
  public final String getPhysicalOptions() {
    return physicalOptions;
  }

  public final void setPhysicalOptions(String value) {
    physicalOptions = value;
  }

  private String refItemId;

  @JsonProperty("RefItemId")
  public final String getRefItemId() {
    return refItemId;
  }

  public final void setRefItemId(String value) {
    refItemId = value;
  }

  private String precision;

  @JsonProperty("Precision")
  public final String getPrecision() {
    return precision;
  }

  public final void setPrecision(String value) {
    precision = value;
  }
}
