/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.generatecomponent;


import com.inspur.edp.bef.bemanager.befexception.BeManagerException;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.lcm.fs.api.IFsService;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import lombok.var;

/**
 * 业务实体构件工具服务类
 *
 * @author hanll02
 */
public final class ComponentGenUtil {

  public static final String ComponentDir = "component";
  private static final String cmpGenExceptionCode = "BeGenerateCmp";

  public static String getComponentAssemblyName(String path) {

    MetadataProjectService projService = SpringBeanUtils.getBean(MetadataProjectService.class);

    MetadataProject projInfo = projService.getMetadataProjInfo(path);
    if (projInfo == null) {
      throw new BeManagerException("", cmpGenExceptionCode, "请先建立元数据工程！", null, ExceptionLevel.Info,
          false);
    }
    return projInfo.getMetadataPackageInfo().getName();
  }

  public static MetadataProject getComponentProjInfo(String path) {

    MetadataProjectService projService = SpringBeanUtils.getBean(MetadataProjectService.class);

    var projInfo = projService.getMetadataProjInfo(path);
    if (projInfo == null) {
      throw new BeManagerException("", cmpGenExceptionCode, "请先建立元数据工程！", null, ExceptionLevel.Info,
          false);
    }
    return projInfo;
  }

  public static String GetComponentNamespace(String path) {

    MetadataService projService = SpringBeanUtils.getBean(MetadataService.class);

    var projInfo = projService.getGspProjectInfo(path);
    if (projInfo == null) {
      throw new BeManagerException("", cmpGenExceptionCode, "请先建立元数据工程！", null, ExceptionLevel.Info,
          false);
    }
    return projInfo.getProjectNameSpace();
  }

  public static String PrepareComponentDir(String bePath) {

    String path = CheckInfoUtil.getCombinePath(bePath, ComponentGenUtil.ComponentDir);

    IFsService fileService = SpringBeanUtils.getBean(IFsService.class);
    if (!fileService.existsAsDir(path)) {
      fileService.createDir(path);
    }
    return path;
  }
}
