/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;
import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaAccessModifier;
import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaClassInfo;

public final class BeMgrActionUtilsGenerator {

  private final GspBusinessEntity be;
  private final String pachageName;
  private final JavaClassInfo classInfo = new JavaClassInfo();

  public BeMgrActionUtilsGenerator(GspBusinessEntity be, String pachageName, String basePath) {
    this.be = be;
    classInfo.setFilePath(basePath);
    this.pachageName = pachageName;
  }

  protected String getClassName() {
    return this.be.getCode() + "MgrActionUtils";
  }

  public final void generate() {
    classInfo.setPackageName(pachageName);
    classInfo.setClassName(getClassName());
    classInfo.getAccessModifiers().add(JavaAccessModifier.Public);
    classInfo.getAccessModifiers().add(JavaAccessModifier.Final);

    generateMgrMethod(be.getCustomMgrActions());
    classInfo.write2File();
  }

  private void generateMgrMethod(BizMgrActionCollection mgrActions) {
    if (mgrActions == null || mgrActions.size() == 0) {
      return;
    }
    for (BizOperation mrAction : mgrActions) {
      new BeMgrActionMethodInvocationGenerator(this.be, (BizMgrAction) mrAction, classInfo)
          .generate();
    }
  }
}
