/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.metadatartevent;

import com.inspur.edp.metadata.rtcustomization.spi.event.IMetadataRtEventListener;
import com.inspur.edp.metadata.rtcustomization.spi.event.MetadataRtEventArgs;

public class BeMetadataRtEventListener implements IMetadataRtEventListener {

  @Override
  public void fireGeneratedMetadataSavingEvent(MetadataRtEventArgs metadataRtEventArgs) {
    return;
//    if (!(metadataRtEventArgs.getMetadata().getContent() instanceof GspBusinessEntity)) {
//      return;
//    }
//
//    DboEntityCheck checker = new DboEntityCheck(
//        (GspBusinessEntity) metadataRtEventArgs.getMetadata().getContent());
//    String msg = checker.checkEntityCoincidence();
//    if (!StringUtils.isEmpty(msg)) {
//      throw new CAFRuntimeException("", "", msg, null, ExceptionLevel.Error);
//    }
  }

  @Override
  public void fireGeneratedMetadataSavedEvent(MetadataRtEventArgs metadataRtEventArgs) {
//    if (!(metadataRtEventArgs.getMetadata().getContent() instanceof GspBusinessEntity)) {
//      return;
//    }
//
//    BefEngineInfoCache.removeBefEngineInfo(
//        ((GspBusinessEntity) metadataRtEventArgs.getMetadata().getContent()).getID());
  }

  @Override
  public void fireMetadataSavingEvent(MetadataRtEventArgs metadataRtEventArgs) {

  }

  @Override
  public void fireMetadataSavedEvent(MetadataRtEventArgs metadataRtEventArgs) {
    // todo 依赖
//    GspMetadata metadata = metadataRtEventArgs.getMetadata();
//    if(!"GSPBusinessEntity".equals(metadataRtEventArgs.getMetadata().getHeader().getType()))
//      return;
//    GspBusinessEntity be = (GspBusinessEntity)metadata.getContent();
//    BefEngineInfoCache.removeBefEngineInfo(be.getID());
//    IGspBeExtendInfoService beInfoService = SpringBeanUtils.getBean(IGspBeExtendInfoService.class);
//    GspBeExtendInfo existing = beInfoService.getBeExtendInfo(be.getID());
//    if(existing!=null) {
//      beInfoService.deleteBeExtendInfo(be.getID());
//    }
//
//    try {
//      List<GspBeExtendInfo> infos = new ArrayList();
//      ProcessMode processMode = metadataRtEventArgs.getProcessMode();
//      BefEngineInfoCache.removeBefEngineInfo(be.getID());
//      GspBeExtendInfo info = buildBeExtendInfo(processMode,be);
//      if(metadata.isExtended()){
//        info.setBeConfigCollectionInfo(null);
//      }
//      // GspBeExtendInfo info = be.getGspBeExtendInfo(processMode);
//      if (be.getGeneratedConfigID() != null) {
//        infos.add(info);
//        beInfoService.saveGspBeExtendInfos(infos);
//      }
//    }catch (Exception e) {
//      throw new RuntimeException("部署解析BE元数据出错：", e);
//    }
  }

  // todo 依赖
//  private GspBeExtendInfo buildBeExtendInfo(ProcessMode processMode, GspBusinessEntity be) {
////    GspBeExtendInfo info = new GspBeExtendInfo();
////    BeConfigCollectionInfo beConfigCollectionInfo = getBefConfigCollectionInfo(be);
////    beConfigCollectionInfo.setProjectType(processMode);
////    info.setId(be.getID());
////    info.setConfigId(be.getGeneratedConfigID());
////    info.setLastChangedOn(new Date());
////    info.setBeConfigCollectionInfo(beConfigCollectionInfo);
////    return info;
//  }

  // todo 依赖
//  private BeConfigCollectionInfo getBefConfigCollectionInfo(GspBusinessEntity be) {
//    CefConfig cefConfig = new CefConfig();
//    cefConfig.setID(be.getGeneratedConfigID());
//    cefConfig.setBEID(be.getID());
//    String nameSpace = be.getCoreAssemblyInfo().getDefaultNamespace();
//    cefConfig.setDefaultNamespace(nameSpace.toLowerCase());
//    BeConfigCollectionInfo beConfigCollectionInfo = new BeConfigCollectionInfo();
//    beConfigCollectionInfo.setConfig(cefConfig);
//    return beConfigCollectionInfo;
//  }

  @Override
  public void fireMetadataDeletingEvent(MetadataRtEventArgs metadataRtEventArgs) {

  }

  @Override
  public void fireMetadataDeletedEvent(MetadataRtEventArgs metadataRtEventArgs) {
// todo 依赖
//    if(!"GSPBusinessEntity".equals(metadataRtEventArgs.getMetadata().getHeader().getType()))
//      return;
//    BefEngineInfoCache.removeBefEngineInfo(metadataRtEventArgs.getMetadata().getHeader().getId());
//    IGspBeExtendInfoRpcService beInfoService = SpringBeanUtils.getBean(IGspBeExtendInfoRpcService.class);
//    GspBeExtendInfo existing = beInfoService.getBeExtendInfo(metadataRtEventArgs.getMetadata().getHeader().getId());
//    if(existing!=null) {
//      beInfoService.deleteBeExtendInfo(metadataRtEventArgs.getMetadata().getHeader().getId());
//    }
  }

}

