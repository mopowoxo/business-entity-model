/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentitydtconsistencychecklistener;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.bizentitydtevent.BizEntityDTEventListener;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.AbstractBeEntityArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.ChangingEntityCodeEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.RemovingEntityEventArgs;
import com.inspur.edp.cef.designtime.api.dtconsistencycheck.ConsistencyCheckEventMessage;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.ArrayList;
import java.util.List;


public
class BizEntityDTConsistencyCheckListener extends BizEntityDTEventListener {

    /**
     * 检查当前BE节点的BE关联依赖信息
     *
     * @param args
     * @return
     */
    @Override
    public RemovingEntityEventArgs removingEntity(RemovingEntityEventArgs args) {
        return (RemovingEntityEventArgs) bizEntityConsistencyCheck(args);
    }

    /**
     * 对关联信息进行检查
     *
     * @param args
     * @return
     */
    protected AbstractBeEntityArgs bizEntityConsistencyCheck(AbstractBeEntityArgs args) {
        String returnMessage = getAssoBEInfos(args.getMetadataPath(), args.getBeId(), args.getBeEntityId());
        if (returnMessage == null || returnMessage.length() == 0) {
            return args;
        }
        ConsistencyCheckEventMessage message = new ConsistencyCheckEventMessage(false, returnMessage);
        args.addEventMessage(message);
        return args;
    }

    /**
     * 获取的关联信息，格式：元数据包【XXX】中BE【XXX】上的字段【XXX】关联了当前BE节点。
     *
     * @param metadataPath 元数据路径
     * @param beId         元数据ID
     * @param beNodeId     be节点ID
     * @return 关联当前BE的关联信息
     */
    protected
    String getAssoBEInfos(String metadataPath, String beId, String beNodeId) {
        MetadataService metadataService = SpringBeanUtils.getBean(com.inspur.edp.lcm.metadata.api.service.MetadataService.class);
        List<GspMetadata> gspMetadataList = metadataService.getMetadataListByRefedMetadataId(metadataPath, beId);
        StringBuilder strBuilder = new StringBuilder();
        gspMetadataList.forEach(gspMetadata -> {
            if (!gspMetadata.getHeader().getType().equals("GSPBusinessEntity"))
                return;
            GspBusinessEntity businessEntity = (GspBusinessEntity) metadataService.loadMetadata(gspMetadata.getHeader().getFileName(), gspMetadata.getRelativePath()).getContent();
            java.util.ArrayList<GspBizEntityObject> bizEntityObjects = businessEntity.getAllNodes();
            for (GspBizEntityObject bizEntityObject : bizEntityObjects) {
                ArrayList<IGspCommonElement> commonElements = bizEntityObject.getAllElementList(true);
                commonElements.forEach(commonElement -> {
                    if (commonElement.getObjectType() == GspElementObjectType.Association) {
                        if(commonElement.getChildAssociations().size() == 0){
                            return;
                        }
                        if (commonElement.getChildAssociations().get(0).getRefObjectID().equals(beNodeId)) {
                            strBuilder.append("工程【").
                                    append(getProjectName(gspMetadata.getRelativePath())).
                                    append("】中BE【").
                                    append(bizEntityObject.getCode())
                                    .append("】上字段【")
                                    .append(commonElement.getCode())
                                    .append("】关联了该BE节点。\n");
                        }
                    }
                });
            }
        });
        if (strBuilder.toString() == null || strBuilder.toString().length() == 0)
            return null;
        return strBuilder.toString();
    }

    /**
     * 获取元数据包名
     *
     * @param metadataPath 元数据路径
     * @return 元数据包名
     */
    protected
    String getProjectName(String metadataPath) {
        MetadataProjectService projectService = SpringBeanUtils
                .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataProjectService.class);
        return projectService.getMetadataProjInfo(metadataPath).getName();
    }
}
