/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.service.cdmparser;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CdmTableInfo {

  public CdmTableInfo() {
  }

  private String tableId;

  @JsonProperty("TableId")
  public final String getTableId() {
    return tableId;
  }

  public final void setTableId(String value) {
    tableId = value;
  }

  private String objectID;

  @JsonProperty("ObjectID")
  public final String getObjectID() {
    return objectID;
  }

  public final void setObjectID(String value) {
    objectID = value;
  }

  private String name;

  @JsonProperty("Name")
  public final String getName() {
    return name;
  }

  public final void setName(String value) {
    name = value;
  }

  private String code;

  @JsonProperty("Code")
  public final String getCode() {
    return code;
  }

  public final void setCode(String value) {
    code = value;
  }

  private int creationDate;

  @JsonProperty("CreationDate")
  public final int getCreationDate() {
    return creationDate;
  }

  public final void setCreationDate(int value) {
    creationDate = value;
  }

  private String creator;

  @JsonProperty("Creator")
  public final String getCreator() {
    return creator;
  }

  public final void setCreator(String value) {
    creator = value;
  }

  private int modificationDate;

  @JsonProperty("ModificationDate")
  public final int getModificationDate() {
    return modificationDate;
  }

  public final void setModificationDate(int value) {
    modificationDate = value;
  }

  private String modifier;

  @JsonProperty("Modifier")
  public final String getModifier() {
    return modifier;
  }

  public final void setModifier(String value) {
    modifier = value;
  }

  private String comment;

  @JsonProperty("Comment")
  public final String getComment() {
    return comment;
  }

  public final void setComment(String value) {
    comment = value;
  }

  private String physicalOptions;

  @JsonProperty("PhysicalOptions")
  public final String getPhysicalOptions() {
    return physicalOptions;
  }

  public final void setPhysicalOptions(String value) {
    physicalOptions = value;
  }


  private java.util.List<CdmColumnInfo> columns;

  @JsonProperty("Columns")
  public final java.util.List<CdmColumnInfo> getColumns() {
    return columns;
  }

  private java.util.List<CdmKey> keys;

  @JsonProperty("Keys")
  public final java.util.List<CdmKey> getKeys() {
    return keys;
  }

  private String primaryItemId;

  @JsonProperty("PrimaryItemId")
  public final String getPrimaryItemId() {
    return primaryItemId;
  }

  public final void setPrimaryItemId(String value) {
    primaryItemId = value;
  }

  public final void addColumn(CdmColumnInfo mColumn) {
    if (columns == null) {
      columns = new java.util.ArrayList<CdmColumnInfo>();
    }
    columns.add(mColumn);
  }

  public final void addKey(CdmKey mKey) {
    if (keys == null) {
      keys = new java.util.ArrayList<CdmKey>();
    }
    keys.add(mKey);
  }
}
