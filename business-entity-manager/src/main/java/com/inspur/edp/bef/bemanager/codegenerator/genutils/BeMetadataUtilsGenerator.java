/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.caf.generator.baseInfo.CompilationUnitInfo;
import com.inspur.edp.caf.generator.item.ClassGenerator;
import com.inspur.edp.caf.generator.item.ClassGeneratorContext;
import java.util.ArrayList;
import org.eclipse.jdt.core.dom.Modifier;

public class BeMetadataUtilsGenerator extends ClassGenerator {

  private final GspBusinessEntity businessEntity;

  public BeMetadataUtilsGenerator(CompilationUnitInfo projectInfo, GspBusinessEntity businessEntity,
      String compAssemblyName) {
    super(projectInfo);
    this.businessEntity = businessEntity;
  }

  @Override
  protected ClassGeneratorContext createClassInfoContext() {
    return new ClassGeneratorContext();
  }

  @Override
  protected ArrayList<Modifier.ModifierKeyword> getAccessModifier() {
    ArrayList<Modifier.ModifierKeyword> list = new ArrayList<>();
    list.add(Modifier.ModifierKeyword.STATIC_KEYWORD);
    list.add(Modifier.ModifierKeyword.PUBLIC_KEYWORD);
    return list;
  }

  @Override
  protected String getName() {
    return businessEntity.getCode() + "Utils";
  }

//    @Override
//    protected ArrayList<ClassMethodGenerator> createMethodGenerators() {
//        ArrayList<ClassMethodGenerator> list = super.createMethodGenerators();
//        if(list ==null)
//            list =new ArrayList<>();
//      for(BizOperation operation:  businessEntity.getBizMgrActions())
//      {
//          BizMgrAction bizMgrAction= (BizMgrAction) operation;
//
//          list.add(new BeMgrActionMethodInvocationGenerator(bizMgrAction));
//      }
//      return list;
//    }
}
