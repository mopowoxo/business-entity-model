/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizVoidReturnType;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.bemgr.BEMgrComponent;
import com.inspur.edp.bef.component.detailcmpentity.bemgr.BEMgrMethodParameter;
import com.inspur.edp.bef.component.enums.ParameterCollectionType;
import com.inspur.edp.bef.component.enums.ParameterMode;
import com.inspur.edp.bef.component.enums.ParameterType;

/**
 * 自定义动作构件生成器
 */
public class BeMgrComponentGenerator extends BaseComponentGenerator {

  public static BeMgrComponentGenerator getInstance() {
    return new BeMgrComponentGenerator();
  }

  private BeMgrComponentGenerator() {
  }

  @Override
  protected GspComponent buildComponent() {
    return new BEMgrComponent();
  }

  ///#region 将BizMgrAction中信息赋值给BEMgrComponent
  @Override
  protected void evaluateComponentInfo(GspComponent component, BizOperation bizMgrAction,
      GspComponent originalComponent) {
    this.originalComponent = originalComponent;
    //1、基本信息
    evaluateComponentBasicInfo((BEMgrComponent) component, (BizMgrAction) bizMgrAction);
    //2、参数信息
    evaluateComponentParameterInfos((BEMgrComponent) component, (BizMgrAction) bizMgrAction);
    //3、返回值信息
    evaluateComponentReturnValueInfo((BEMgrComponent) component, (BizMgrAction) bizMgrAction);
  }

  private void evaluateComponentBasicInfo(BEMgrComponent component, BizMgrAction bizMgrAction) {
    if (!CheckInfoUtil.checkNull(bizMgrAction.getComponentId())) {
      component.setComponentID(bizMgrAction.getComponentId());
    }
    component.setComponentCode(bizMgrAction.getCode());
    component.setComponentName(bizMgrAction.getName());
    component.setComponentDescription(bizMgrAction.getDescription());
    component.getBeMgrMethod().setDotnetAssembly(this.assemblyName);

    String packageName = JavaModuleImportPackage(this.nameSpace);
    packageName = String
        .format("%1$s%2$s", packageName, JavaCompCodeNames.MgrActionNameSpaceSuffix);

    if (this.originalComponent != null) {
      component.getBeMgrMethod()
          .setDotnetClassName(this.originalComponent.getMethod().getDotnetClassName());
      component.getMethod().setClassName(
          JavaModuleClassName(this.originalComponent.getMethod().getDotnetClassName(),
              packageName));
    } else {
      component.getBeMgrMethod().setDotnetClassName(ComponentClassNameGenerator
          .generateBEMgrComponentClassName(this.nameSpace, this.objCode + bizMgrAction.getCode()));
      component.getMethod().setClassName(JavaModuleClassName(ComponentClassNameGenerator
              .generateBEMgrComponentClassName(this.nameSpace, this.objCode + bizMgrAction.getCode()),
          packageName));
    }

    // component.GetMethod().JavaPackageName = packageName;

  }


  private void evaluateComponentParameterInfos(BEMgrComponent component,
      BizMgrAction bizMgrAction) {
    for (Object parameter : bizMgrAction.getParameters()) {
      evaluateComponentParameterInfo((IBizParameter) parameter, component);
    }
  }

  private void evaluateComponentParameterInfo(IBizParameter bizParameter,
      BEMgrComponent component) {
    BEMgrMethodParameter tempVar = new BEMgrMethodParameter();
    tempVar.setID(bizParameter.getID());
    tempVar.setParamCode(bizParameter.getParamCode());
    tempVar.setParamName(bizParameter.getParamName());
    tempVar.setParamDescription(bizParameter.getParamDescription());
    tempVar.setMode(ParameterMode.valueOf(bizParameter.getMode().name().toString()));
    tempVar.setParameterCollectionType(ParameterCollectionType
        .valueOf(bizParameter.getCollectionParameterType().name().toString()));
    tempVar
        .setParameterType(ParameterType.valueOf(bizParameter.getParameterType().name().toString()));
    tempVar.setAssembly(bizParameter.getAssembly());
    tempVar.setClassName(bizParameter.getClassName());
    tempVar.setDotnetClassName(((BizParameter) bizParameter).getNetClassName());
    BEMgrMethodParameter parameter = tempVar;

    component.getBeMgrMethod().getParameters().add(parameter);
  }

  private void evaluateComponentReturnValueInfo(BEMgrComponent component,
      BizMgrAction bizMgrAction) {
    //类型
    if (bizMgrAction.getReturnValue() instanceof BizVoidReturnType) {
      component.getBeMgrMethod().setReturnValue(new VoidReturnType());
    } else {
      component.getBeMgrMethod().getReturnValue().setParameterCollectionType(ParameterCollectionType
          .valueOf(bizMgrAction.getReturnValue().getCollectionParameterType().name().toString()));
      component.getBeMgrMethod().getReturnValue().setParameterType(ParameterType
          .valueOf(bizMgrAction.getReturnValue().getParameterType().name().toString()));
      component.getBeMgrMethod().getReturnValue()
          .setAssembly(bizMgrAction.getReturnValue().getAssembly());
      component.getBeMgrMethod().getReturnValue()
          .setClassName(bizMgrAction.getReturnValue().getClassName());
      component.getBeMgrMethod().getReturnValue()
          .setDotnetClassName(bizMgrAction.getReturnValue().getNetClassName());
    }
    //基本信息
    component.getBeMgrMethod().getReturnValue().setID(bizMgrAction.getReturnValue().getID());
    component.getBeMgrMethod().getReturnValue()
        .setParamDescription(bizMgrAction.getReturnValue().getParamDescription());
  }

  ///#endregion
}
