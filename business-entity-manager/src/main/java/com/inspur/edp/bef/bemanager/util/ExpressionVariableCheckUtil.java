/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.util;

import com.google.common.collect.Lists;
import java.util.List;

/**
 * 用于校验表达式默认值的工具类
 */
public class ExpressionVariableCheckUtil {

  /**
   * 上一年
   */
  private static final String LAST_YEAR = "GetSessionValue(\\\"LastYear\\\")";

  /**
   * 上一月
   */
  private static final String LAST_MONTH = "GetSessionValue(\\\"LastMonth\\\")";

  /**
   * 当前年
   */
  private static final String CURRENT_YEAR = "GetSessionValue(\\\"CurrentYear\\\")";

  /**
   * 当前月
   */
  private static final String CURRENT_MONTH = "GetSessionValue(\\\"CurrentMonth\\\")";

  /**
   * 当前时间
   */
  private static final String CURRENT_DATE_TIME = "GetSessionValue(\\\"CurrentDateTime\\\")";

  /**
   * 登陆时间
   */
  private static final String LOGIN_TIME = "GetSessionValue(\\\"LoginTime\\\")";

  /**
   * 登录用户ID
   */
  private static final String USER_ID = "GetSessionValue(\\\"UserId\\\")";

  /**
   * 登陆用户编号
   */
  private static final String USER_CODE = "GetSessionValue(\\\"UserCode\\\")";

  /**
   * 登陆用户名称
   */
  private static final String USER_NAME = "GetSessionValue(\\\"UserName\\\")";

  /**
   * 系统组织标识
   */
  private static final String SYS_ORG_ID = "GetSessionValue(\\\"SysOrgId\\\")";

  /**
   * 系统组织编号
   */
  private static final String SYS_ORG_CODE = "GetSessionValue(\\\"SysOrgCode\\\")";

  /**
   * 系统组织名称
   */
  private static final String SYS_ORG_NAME = "GetSessionValue(\\\"SysOrgName\\\")";

  /**
   * 当前语言
   */
  private static final String LANGUAGE = "GetSessionValue(\\\"Language\\\")";

  /**
   * 日期型表达式集合
   */
  private static final List<String> dateList =
      Lists.newArrayList(LOGIN_TIME, CURRENT_DATE_TIME);

  /**
   * 文本类型表达式集合
   */
  private static final List<String> textList =
      Lists.newArrayList(USER_ID, USER_CODE, USER_NAME, SYS_ORG_ID, SYS_ORG_CODE, SYS_ORG_NAME,
          LANGUAGE,
          CURRENT_MONTH, CURRENT_YEAR, LAST_MONTH, LAST_YEAR);

  /**
   * 判断表达式是否是日期类型x
   *
   * @param expression 表达式
   * @return 是否日期类型
   */
  public static boolean isDateType(String expression) {
    if (CheckInfoUtil.checkNull(expression)) {
      return false;
    }
    for (String dateString : dateList) {
      if (expression.contains(dateString)) {
        return true;
      }
    }
    return false;
  }

  /**
   * 判断表达式是否是文本类型
   *
   * @param expression 表达式
   * @return 是否文本类型
   */
  public static boolean isTextType(String expression) {
    if (CheckInfoUtil.checkNull(expression)) {
      return false;
    }
    for (String dateString : textList) {
      if (expression.contains(dateString)) {
        return true;
      }
    }
    return false;
  }
}
