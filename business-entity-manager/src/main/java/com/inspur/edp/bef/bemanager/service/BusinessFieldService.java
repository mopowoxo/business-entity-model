/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.nocode.BusinessField;
import com.inspur.edp.udt.designtime.api.nocode.IBusinessFieldService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.UUID;

public class BusinessFieldService {

  public GspBizEntityElement handelElements(String info) {

    JsonNode node = getNodeFromJson(info);
    String id = node.get("ID").textValue();
    String labelId = node.get("LabelID").textValue();
    String name = node.get("Name").textValue();
    String businessFieldId = node.get("BusinessFieldID").textValue();

    IBusinessFieldService businessFieldService = SpringBeanUtils
        .getBean(IBusinessFieldService.class);
    BusinessField businessField = businessFieldService.getBusinessField(businessFieldId);

    GspBizEntityElement gspBizEntityElement = new GspBizEntityElement();
    gspBizEntityElement.setID(id);
    gspBizEntityElement.setLabelID(labelId);
    gspBizEntityElement.setCode(labelId);
    gspBizEntityElement.setName(name);
    gspBizEntityElement.setRefBusinessFieldId(businessFieldId);
    gspBizEntityElement.setRefBusinessFieldName(businessField.getName());
    //todo其余属性？
//        gspBizEntityElement.setPrecision();
    if (businessField.getUnifiedDataTypeDef() instanceof SimpleDataTypeDef) {
      SimpleDataTypeDef simpleDataTypeDef = (SimpleDataTypeDef) businessField
          .getUnifiedDataTypeDef();
      if (simpleDataTypeDef.getObjectType() == GspElementObjectType.Association) {
        gspBizEntityElement.setObjectType(GspElementObjectType.Association);
        GspAssociation gspAssociation = simpleDataTypeDef.getChildAssociations().get(0);

        GspAssociation association = gspAssociation.clone();
        association.setId(UUID.randomUUID().toString());
        association.getRefElementCollection().clear();
        if (gspAssociation.getRefElementCollection() != null
            && gspAssociation.getRefElementCollection().size() > 0) {
          for (IGspCommonField field : gspAssociation.getRefElementCollection()) {
            GspBizEntityElement gspBizElement = new GspBizEntityElement();
            gspBizElement.setID(UUID.randomUUID().toString());
            gspBizElement.setCode(field.getCode());
            gspBizElement.setLabelID(labelId + "_" + field.getCode());
            gspBizElement.setName(field.getName());
            gspBizElement.setMDataType(field.getMDataType());
            gspBizElement.setLength(field.getLength());
            gspBizElement.setIsRefElement(true);
            gspBizElement.setEnableRtrim(false);
            gspBizElement.setRefElementId(field.getRefElementId());
            gspBizElement.setUdtID(field.getUdtID());
            gspBizElement.setUdtName(field.getUdtName());
            gspBizElement.setIsUdt(field.getIsUdt());
            gspBizElement.setParentAssociation(association);
            association.getRefElementCollection().add(gspBizElement);
          }
        }
        association.getKeyCollection().get(0).setTargetElement(id);
        GspAssociationCollection associationCollection = new GspAssociationCollection();
        associationCollection.add(association);
        gspBizEntityElement.setChildAssociations(associationCollection);
      }
    }

    return gspBizEntityElement;
  }

  private JsonNode getNodeFromJson(String json) {
    ObjectMapper mapper = new ObjectMapper();
    JsonNode node = null;

    try {
      node = mapper.readTree(json);
    } catch (JsonProcessingException e) {
      throw new RuntimeException("json串解析失败！");
    }
    return node;
  }
}
