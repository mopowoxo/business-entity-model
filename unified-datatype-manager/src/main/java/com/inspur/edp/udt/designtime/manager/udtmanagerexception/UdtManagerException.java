/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager.udtmanagerexception;


import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class UdtManagerException extends CAFRuntimeException {

  // 异常标识，用于前端截取异常信息时辨识
  private String errorToken = "#GSPBefError# ";

  public UdtManagerException(String exceptionCode, String resourceFile, String messageCode,
      RuntimeException innerException, ExceptionLevel level, boolean isBizException) {
    super(exceptionCode, resourceFile, messageCode, innerException, level, isBizException);
  }


}
