/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.controlrule;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.AbstractControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.RangeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;

import java.util.HashMap;
import java.util.Map;

public class CmControlRule extends RangeControlRule {
    public CmControlRule() {
        createMainEntityControlRule();
    }

    public ControlRuleItem getNameControlRule() {
        return super.getControlRule(CmRuleNames.Name);
    }

    public void setNameControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CmRuleNames.Name, ruleItem);
    }

    public ControlRuleItem getCodeControlRule() {
        return super.getControlRule(CmRuleNames.Code);
    }

    public void setCodeControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CmRuleNames.Code, ruleItem);
    }

    protected CmEntityControlRule createMainEntityControlRule(){
        return new CmEntityControlRule();
    }

    public CmEntityControlRule getMainEntityControlRule() {
        if (!getChildRules().containsKey(CommonDataTypeRuleNames.MainObjectRule)) {
            return null;
        }
        Map<String, AbstractControlRule> rules = getChildRules().get(CommonDataTypeRuleNames.MainObjectRule);
        if (rules == null) {
            rules = new HashMap<>();
            getChildRules().put(CommonDataTypeRuleNames.MainObjectRule, rules);
        }
        return (CmEntityControlRule) rules.get(CommonDataTypeRuleNames.MainObjectRule);
    }

    public void setMainEntityControlRule(CmEntityControlRule mainObjRule){
        if (!getChildRules().containsKey(CommonDataTypeRuleNames.MainObjectRule)) {
            getChildRules().put(CommonDataTypeRuleNames.MainObjectRule, new HashMap<>());
        }
        getChildRules().get(CommonDataTypeRuleNames.MainObjectRule).put(CommonDataTypeRuleNames.MainObjectRule, mainObjRule);
    }
}
