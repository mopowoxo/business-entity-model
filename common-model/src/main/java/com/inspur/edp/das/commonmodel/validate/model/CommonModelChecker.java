/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.validate.model;

import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;
import com.inspur.edp.cef.designtime.api.variable.inspaction.VariableChecker;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;
import com.inspur.edp.das.commonmodel.validate.object.CMObjectChecker;
import org.springframework.util.StringUtils;

public abstract class CommonModelChecker {

    protected void checkCM(GspCommonModel commonModel) {
        //节点编号
        checkModelCode(commonModel);
        //变量
        checkVar(commonModel.getVariables());
        //节点动作
        checkExtension(commonModel);
        //实体动作，字段。以及子集
        checkObjExtension(commonModel.getMainObject());
    }
    //节点编号
    private void checkModelCode(GspCommonModel gspCommonModel) {
        if(StringUtils.isEmpty(gspCommonModel.getCode())){
            CheckUtil.exception("业务实体编号不允许为空。");
        }
        if(StringUtils.isEmpty(gspCommonModel.getName())){
            CheckUtil.exception("业务实体名称不允许为空。");
        }
        if(StringUtils.isEmpty(gspCommonModel.getGeneratedConfigID())){
            CheckUtil.exception("业务实体ConfigId不允许为空。");
        }
        if (!CheckUtil.isLegality(gspCommonModel.getCode())){
            CheckUtil.exception("业务实体编号： " + gspCommonModel.getCode() + "不允许使用Java关键字。");
        }
        //这个在保存前metadatas有了判断了
//        if (CheckUtil.isValidInput(gspCommonModel.getCode())){
//            CheckUtil.exception("业务实体编号： " + gspCommonModel.getCode() + "不允许使用字母、数字、_以外的字符。");
//        }
//        if (CheckUtil.isStartWithChar(gspCommonModel.getCode())){
//            CheckUtil.exception("业务实体编号： " + gspCommonModel.getCode() + "非字母开头。");
//        }
    }

    private void checkVar(CommonVariableEntity variableEntity) {
       VariableChecker.getInstance().checkVariable(variableEntity);
    }

    protected abstract void checkExtension(GspCommonModel gspCommonModel);

    private  void checkObjExtension(GspCommonObject object){
        getCMObjectChecker().checkObj(object);
    }

    protected abstract CMObjectChecker getCMObjectChecker();
}
