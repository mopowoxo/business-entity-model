/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.json.increment;

import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;
import com.inspur.edp.das.commonmodel.json.element.CmElementDeserializer;

public abstract class CommonElementIncrementDeserializer extends GspFieldIncrementDeserializer {
    @Override
    protected final CefFieldDeserializer getFieldDeserializer() {
        return getCmElementDeserializer();
    }

    protected abstract CmElementDeserializer getCmElementDeserializer();
}
