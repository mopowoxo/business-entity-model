/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.controlrule.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.parser.CommonFieldRuleParser;
import com.inspur.edp.das.commonmodel.controlrule.CmFieldControlRule;

public class CmFieldRuleParser<T extends CmFieldControlRule> extends CommonFieldRuleParser<T> {

    @Override
    protected final boolean readCommonFieldRuleExtendProperty(T controlRule, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readCommonFieldRuleExtendProperty(controlRule, propName, jsonParser, deserializationContext))
            return true;
        return readCmFieldRuleExtendProperty(controlRule, propName, jsonParser, deserializationContext);
    }

    protected boolean readCmFieldRuleExtendProperty(T controlRule, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    @Override
    protected final CmFieldControlRule createCommonFieldRuleDefinition() {
        return createCmFieldRuleDefinition();
    }

    protected CmFieldControlRule createCmFieldRuleDefinition() {
        return new CmFieldControlRule();
    }
}
